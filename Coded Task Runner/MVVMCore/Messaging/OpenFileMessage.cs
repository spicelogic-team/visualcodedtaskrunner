﻿using System;
using Microsoft.Win32;

namespace SpiceLogic.CodedTaskRunner.MVVMCore.Messaging
{
    internal class OpenFileMessage
    {
        /// <summary>
        /// Gets or sets the dialog.
        /// </summary>
        public OpenFileDialog Dialog { get; set; }

        /// <summary>
        /// Gets or sets the call back.
        /// </summary>
        public Action<string> CallBackWithSelectedFilePath { get; set; }
    }
}