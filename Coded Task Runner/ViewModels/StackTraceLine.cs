﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class StackTraceLine
    {
        private static string _solutionFolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="StackTraceLine"/> class.
        /// </summary>
        /// <param name="theLine">The line.</param>
        public StackTraceLine(string theLine)
        {
            if (string.IsNullOrEmpty(_solutionFolder))
            {
                string assemblyLocation = AppDomain.CurrentDomain.BaseDirectory;
                _solutionFolder = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(assemblyLocation))));
            }
         
            string[] splitArray = new string[0];
            try
            {
                splitArray = Regex.Split(theLine, @"\) in ", RegexOptions.IgnoreCase);
            }
            catch 
            {
                // Syntax error in the regular expression
            }

            if (splitArray.Length > 0)
            {
                this.CallerInfo = splitArray[0] + ")";

                if (splitArray.Length > 1)
                {
                    string theLastPart = splitArray[1];

                    string[] lastArrayParts = new string[0];
                    try
                    {
                        lastArrayParts = Regex.Split(theLastPart, ":line ", RegexOptions.IgnoreCase);
                    }
                    catch
                    {
                        // Syntax error in the regular expression
                    }

                    if (lastArrayParts.Length > 0)
                    {
                        this.FileLocation = lastArrayParts[0];
                        
                        try
                        {
                            if (_solutionFolder != null && this.FileLocation.StartsWith(_solutionFolder, StringComparison.OrdinalIgnoreCase))
                                this.RelativeFileLocation = this.FileLocation.Remove(0, _solutionFolder.Length);
                        }
                        catch (ArgumentException)
                        {
                            // Syntax error in the regular expression
                        }

                        int theLineNumber;
                        if (lastArrayParts.Length > 1 && int.TryParse(lastArrayParts[1], out theLineNumber))
                            this.LineNumber = theLineNumber;
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.RelativeFileLocation) && this.LineNumber > 0)
            {
                this.LineTooltip = $"{this.RelativeFileLocation} (line : {this.LineNumber})";
                this.IsMyCode = true;
            }
        }

        /// <summary>
        /// Gets or sets the relative file location.
        /// </summary>
        /// <value>
        /// The relative file location.
        /// </value>
        public string RelativeFileLocation { get; set; }

        /// <summary>
        /// Gets or sets the caller information.
        /// </summary>
        /// <value>
        /// The caller information.
        /// </value>
        public string CallerInfo { get; set; }

        /// <summary>
        /// Gets or sets the file location.
        /// </summary>
        /// <value>
        /// The file location.
        /// </value>
        public string FileLocation { get; set; }

        /// <summary>
        /// Gets or sets the line number.
        /// </summary>
        /// <value>
        /// The line number.
        /// </value>
        public int LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the line tooltip.
        /// </summary>
        /// <value>
        /// The line tooltip.
        /// </value>
        public string LineTooltip { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is my code.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is my code; otherwise, <c>false</c>.
        /// </value>
        public bool IsMyCode { get; set; }
    }
}