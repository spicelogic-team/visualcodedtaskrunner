using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace SpiceLogic.CodedTaskRunner.Utils
{
    internal static class Serialization
    {
        /// <summary>
        /// Serializes the specified obj.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The obj.</param>
        /// <param name="serializedPath">The serialized path.</param>
        public static void Serialize<T>(this T obj, string serializedPath) where T : class
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj), "obj is null");

            if (string.IsNullOrWhiteSpace(serializedPath))
                throw new ArgumentNullException(nameof(serializedPath), "serializedPath argument is null/empty string/white space");
          
            DataContractSerializer ds = new DataContractSerializer(typeof(T));

            using (Stream s = File.Create(serializedPath))
            {
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = false };

                using (XmlWriter xmlWriter = XmlWriter.Create(s, settings))
                {
                    ds.WriteObject(xmlWriter, obj);
                   
                    xmlWriter.Close();
                }
            }
        }

        /// <summary>
        /// Serializes the specified obj.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static string Serialize<T>(this T obj) where T : class
        {
            if (obj == null)
                return null;

            using (MemoryStream memStream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = false };

                using (XmlWriter xmlWriter = XmlWriter.Create(memStream, settings))
                {
                    new DataContractSerializer(typeof(T)).WriteObject(xmlWriter, obj);
                    xmlWriter.Close();
                }

                memStream.Close();

                string xml = Encoding.UTF8.GetString(memStream.GetBuffer());
                xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
                xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1));

                return xml;
            }
        }

        /// <summary>
        /// Deserializes from XML STR.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        public static T DeserializeFromXmlStr<T>(string xml)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(xml))
                    return default(T);//null; // IF where T : class were not used, then, use return default(T);

                DataContractSerializer xs = new DataContractSerializer(typeof(T));

                using (StringReader stringReader = new StringReader(xml))
                {
                    using (XmlTextReader xmlReader = new XmlTextReader(stringReader))
                    {
                        T obj = (T)xs.ReadObject(xmlReader);

                        xmlReader.Close();

                        return obj;
                    }
                }
            }
            catch
            {
                return default(T);
            }
        }
    }
}