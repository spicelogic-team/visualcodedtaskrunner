﻿using System.ComponentModel;
using System.Threading;

namespace SpiceLogic.CodedTaskRunner
{
    /// <summary>
    /// This is a Manual Instruction Task
    /// </summary>
    public class ManualInstructionTask : CodedTaskBase
    {
        internal string Instruction { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualInstructionTask"/> class.
        /// </summary>
        /// <param name="instruction">The instruction.</param>
        public ManualInstructionTask(string instruction)
        {
            this.Instruction = instruction;
        }

        public override string Execute(CancellationToken cancellationToken)
        {
            return "";
        }

        /// <summary>
        /// Cancels the task.
        /// </summary>
        /// <buildflag>Exclude from Online</buildflag>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new void Cancel()
        {
        }
    }
}