﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using SpiceLogic.CodedTaskRunner.Annotations;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// Activity Log
    /// </summary>
    [DataContract]
    public class ActivityLog : INotifyPropertyChanged
    {
        private const int FirstLineTrimmingLength = 255;
        private int _id;
        private DateTime _eventDateTime;
        private string _message;
        private LogTypes _logType;
        private bool? _hasManyLines;

        /// <summary>
        /// The Log Types
        /// </summary>
        public enum LogTypes
        {
            /// <summary>
            /// The event
            /// </summary>
            Event,
            /// <summary>
            /// The information
            /// </summary>
            Info,
            /// <summary>
            /// The warning
            /// </summary>
            Warning,
            /// <summary>
            /// The task execution result
            /// </summary>
            TaskExecutionResult,
            /// <summary>
            /// The exception
            /// </summary>
            Exception
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityLog"/> class.
        /// </summary>
        public ActivityLog()
        {
            this.EventDateTime = DateTime.Now;

        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember(Order = 0)]
        public int ID
        {
            get => _id;
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the type of the log.
        /// </summary>
        /// <value>
        /// The type of the log.
        /// </value>
        [DataMember(Order = 1)]
        public LogTypes LogType
        {
            get => _logType;
            set
            {
                if (value == _logType) return;
                _logType = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the event date time.
        /// </summary>
        /// <value>
        /// The event date time.
        /// </value>
        [DataMember(Order = 2)]
        public DateTime EventDateTime
        {
            get => _eventDateTime;
            set
            {
                if (value.Equals(_eventDateTime)) return;
                _eventDateTime = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [DataMember(Order = 3)]
        public string Message
        {
            get => _message;
            set
            {
                if (value == _message) return;
                _message = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the message first line.
        /// </summary>
        /// <value>
        /// The message first line.
        /// </value>
        [IgnoreDataMember]
        public string MessageFirstLine
        {
            get
            {
                string firstLine = AllLinesForMessage[0];
                if (string.IsNullOrWhiteSpace(firstLine))
                    return string.Empty;
                if (firstLine.Length > FirstLineTrimmingLength)
                {
                    firstLine = firstLine.Substring(0, FirstLineTrimmingLength);
                    firstLine += " ...";
                    _hasManyLines = true;
                }
                return firstLine;
            }
        }

        /// <summary>
        /// Gets all lines for message.
        /// </summary>
        /// <value>
        /// All lines for message.
        /// </value>
        [IgnoreDataMember]
        public string[] AllLinesForMessage
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._message))
                    return new string[0];
                string[] lines = this._message.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                return lines;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has many lines.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has many lines; otherwise, <c>false</c>.
        /// </value>
        [IgnoreDataMember]
        public bool HasManyLines
        {
            get
            {
                if (!_hasManyLines.HasValue)
                    _hasManyLines = AllLinesForMessage.Length > 1;
                return _hasManyLines.Value;
            }
            set => _hasManyLines = value;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Ons the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}