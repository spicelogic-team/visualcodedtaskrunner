﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;

namespace SpiceLogic.CodedTaskRunner.Behaviors
{
    /// <summary>
    /// 
    /// </summary>
    public static class HyperlinkExternalUrlBehavior
    {
        /// <summary>
        /// The is external property
        /// </summary>
        public static readonly DependencyProperty IsExternalProperty =
            DependencyProperty.RegisterAttached("IsExternal", typeof(bool), typeof(HyperlinkExternalUrlBehavior), new UIPropertyMetadata(false, OnIsExternalChanged));

        /// <summary>
        /// Gets the is external.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static bool GetIsExternal(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsExternalProperty);
        }

        /// <summary>
        /// Sets the is external.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetIsExternal(DependencyObject obj, bool value)
        {
            obj.SetValue(IsExternalProperty, value);
        }

        private static void OnIsExternalChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            Hyperlink hyperlink = sender as Hyperlink;
            if (hyperlink != null)
            {
                if ((bool)args.NewValue)
                    hyperlink.RequestNavigate += Hyperlink_RequestNavigate;
                else
                    hyperlink.RequestNavigate -= Hyperlink_RequestNavigate;
            }
        }

        private static void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("Error opening url : {0}{1}{1}{2}", e.Uri.AbsoluteUri, Environment.NewLine, err.Message));
            }
            finally
            {
                e.Handled = true;
            }
        }
    }
}