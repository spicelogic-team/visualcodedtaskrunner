﻿using System;
using System.Windows;
using SpiceLogic.CodedTaskRunner.MVVMCore.Command;

namespace SpiceLogic.CodedTaskRunner.Models
{
    internal class DecoratedUserToolbarButton : UserToolbarButton
    {
        private RelayCommand _clickCommand;
        private readonly UserToolbarButton _baseObj;

        /// <summary>
        /// Initializes a new instance of the <see cref="DecoratedUserToolbarButton"/> class.
        /// </summary>
        /// <param name="baseObj">The base object.</param>
        public DecoratedUserToolbarButton(UserToolbarButton baseObj)
        {
            this._baseObj = baseObj;

            this.ButtonText = baseObj.ButtonText;
            this.ImageDimension = baseObj.ImageDimension;
            this.ToolbarImageSource = baseObj.ToolbarImageSource;
            this.Tooltip = baseObj.Tooltip;
        }

        /// <summary>
        /// Gets the click command.
        /// </summary>
        public RelayCommand ClickCommand
        {
            get
            {
                if (this._baseObj.ClickAction == null)
                    return new RelayCommand(() => {});
                return _clickCommand ?? (_clickCommand =  new RelayCommand(() =>
                {
                    try
                    {
                        this._baseObj.ClickAction.Invoke();
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show(err.Message, $"Exception from your Button '{this.ButtonText}'");
                    }
                }));
            }
        }
    }
}