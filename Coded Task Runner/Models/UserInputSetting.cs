﻿using System.Collections.Generic;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UserInputSetting
    {
        /// <summary>
        /// 
        /// </summary>
        public string SelectionInputLabel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<UserInputItem> SelectionItems { get; set; }

        public string SelectedItemDisplayText { get; set; }
    }
}