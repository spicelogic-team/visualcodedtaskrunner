﻿using System.Windows;
using SpiceLogic.CodedTaskRunner.Models;
using SpiceLogic.CodedTaskRunner.ViewModels;

namespace SpiceLogic.CodedTaskRunner.Views.UserControls.TaskStatusVisualizers
{
    /// <summary>
    /// Interaction logic for FaultedVisualizer.xaml
    /// </summary>
    public partial class FaultedVisualizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FaultedVisualizer"/> class.
        /// </summary>
        public FaultedVisualizer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the btnException control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnException_Click(object sender, RoutedEventArgs e)
        {
            CodedTaskReport theDataContext = (CodedTaskReport)this.DataContext;
            DialogFactory.ShowDialog(DialogFactory.DialogNames.TaskException, 
                new ExceptionViewModel(theDataContext.ThrownException,
                    $"Exception occured in CodedTask # {theDataContext.Sequence}"));
        }
    }
}
