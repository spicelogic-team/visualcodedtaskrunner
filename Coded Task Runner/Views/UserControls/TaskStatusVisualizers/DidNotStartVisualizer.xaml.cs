﻿using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner.Views.UserControls.TaskStatusVisualizers
{
    /// <summary>
    /// Interaction logic for DidNotStartVisualizer.xaml
    /// </summary>
    public partial class DidNotStartVisualizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DidNotStartVisualizer"/> class.
        /// </summary>
        public DidNotStartVisualizer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            imgStatus.ToolTip = this.DataContext is CodedTaskReport theTaskInfo && theTaskInfo.IsAttendedTask ? "Did not start (Attended CodedTask)" : "Did not start";
        }       
    }
}
