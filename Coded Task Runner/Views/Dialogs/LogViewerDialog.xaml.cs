﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SpiceLogic.CodedTaskRunner.Models;
using SpiceLogic.CodedTaskRunner.MVVMCore.Messaging;
using SpiceLogic.CodedTaskRunner.ViewModels;

namespace SpiceLogic.CodedTaskRunner.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for LogViewerDialog.xaml
    /// </summary>
    internal partial class LogViewerDialog
    {
        private MainViewModel _theViewModel;

        public LogViewerDialog()
        {
            InitializeComponent();
            Messenger.Default.Register<DialogMessage>(this, showDialogTask);
        }

        /// <summary>
        /// Shows the dialog task.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void showDialogTask(DialogMessage obj)
        {
            MessageBoxResult theMsgBoxShowResult = MessageBox.Show(this, obj.Content, obj.Caption, obj.Button);
            obj.Callback(theMsgBoxShowResult);
        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _theViewModel = (MainViewModel)this.DataContext;
        }

/*
        /// <summary>
        /// CTRLs the C copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ListView lb = (ListView)(sender);
            ActivityLog selected = (ActivityLog) lb.SelectedItem;
            if (selected != null)
                Clipboard.SetText(selected.Message);
        }
*/

/*
        /// <summary>
        /// CTRLs the C copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
*/

        /// <summary>
        /// Rights the click copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            ActivityLog selected = (ActivityLog) mi.DataContext;
            if (selected != null)
                Clipboard.SetText(selected.Message);
        }

        /// <summary>
        /// Rights the click copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister<DialogMessage>(this, showDialogTask);
        }

        /// <summary>
        /// Handles the KeyDown event of the txtSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                _theViewModel.SearchLogCommand.Execute(e);
        }
    }
}
