﻿using System;
using System.ComponentModel;
using System.Threading;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner
{
    /// <summary>
    /// This is the Base Class for an individual Coded Task.
    /// </summary>
    public abstract class CodedTaskBase : IDisposable
    {
        internal event EventHandler<ProgressChangedEventArgs> ProgressChanged;
        internal event EventHandler<StatusMessageUpdateEventArgs> StatusMessageUpdated;

        /// <summary>
        ///  Executes the task Asynchronously.
        ///  </summary><returns>CodedTask Execution Result</returns>
        public abstract string Execute(CancellationToken cancellationToken);

        /// <summary>
        ///  Cancels the task.
        ///  </summary>
        public virtual void Cancel()
        {
        }

        /// <summary>
        /// Reports the progress of an individual task.
        /// </summary>
        /// <param name="progerssPercent">The progerss percent.</param>
        protected void ReportProgress(int progerssPercent)
        {
            ProgressChanged?.Invoke(this, new ProgressChangedEventArgs(progerssPercent, null));
        }

        /// <summary>
        /// Updates the status message when an individual task is running.
        /// </summary>
        /// <param name="statusMessage">The status message.</param>
        protected void UpdateStatusMessage(string statusMessage)
        {
            StatusMessageUpdated?.Invoke(this, new StatusMessageUpdateEventArgs(statusMessage));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
        }
    }
}
