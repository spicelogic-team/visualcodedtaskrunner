﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for TaskCompleteResultDialog.xaml
    /// </summary>
    internal partial class TaskCompleteResultDialog
    {
        public TaskCompleteResultDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// CTRLs the C copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ListBox lb = (ListBox)(sender);
            object selected = lb.SelectedItem;
            if (selected != null)
                Clipboard.SetText(selected.ToString());
        }

        /// <summary>
        /// CTRLs the C copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Rights the click copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            object selected = mi.DataContext;
            if (selected != null)
                Clipboard.SetText(selected.ToString());
        }

        /// <summary>
        /// Rights the click copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Rights the name of the click copy CMD executed task.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdExecutedTaskName(object sender, ExecutedRoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            CodedTaskReport selected = (CodedTaskReport) mi.DataContext;
            if (selected != null)
                Clipboard.SetText(selected.Description);
        }
    }
}
