﻿using System;
using System.ComponentModel;
using System.Threading;

namespace SpiceLogic.CodedTaskRunner
{
    /// <summary>
    /// This is a Simple Action Task that can be used within the Task Index.
    /// </summary>
    public class SimpleActionTask : CodedTaskBase
    {
        internal string TaskDescription { get; private set; }
        internal Func<CancellationToken, string> TaskAction { get; private set; }
        internal bool IsAttendedTask { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleActionTask"/> class.
        /// </summary>
        /// <param name="taskDescription">The task description.</param>
        /// <param name="action">The action.</param>
        /// <param name="isAttendedTask">if set to <c>true</c> [is attended task].</param>
        public SimpleActionTask(string taskDescription, Func<CancellationToken, string> action, bool isAttendedTask = false)
        {
            this.TaskDescription = taskDescription;
            this.TaskAction = action;
            this.IsAttendedTask = isAttendedTask;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleActionTask"/> class.
        /// </summary>
        /// <param name="taskDescription">The task description.</param>
        /// <param name="action">The action.</param>
        /// <param name="isAttendedTask">if set to <c>true</c> [is attended task].</param>
        public SimpleActionTask(string taskDescription, Action<CancellationToken> action, bool isAttendedTask = false)
        {
            this.TaskDescription = taskDescription;
            this.TaskAction = cancellationToken => { action(cancellationToken); return string.Empty; };
            this.IsAttendedTask = isAttendedTask;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleActionTask"/> class.
        /// </summary>
        /// <param name="taskDescription">The task description.</param>
        /// <param name="action">The action.</param>
        /// <param name="isAttendedTask">if set to <c>true</c> [is attended task].</param>
        public SimpleActionTask(string taskDescription, Action action, bool isAttendedTask = false)
        {
            this.TaskDescription = taskDescription;
            this.TaskAction = cancellationToken => { action(); return string.Empty; };
            this.IsAttendedTask = isAttendedTask;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleActionTask"/> class.
        /// </summary>
        /// <param name="taskDescription">The task description.</param>
        /// <param name="action">The action.</param>
        /// <param name="isAttendedTask">if set to <c>true</c> [is attended task].</param>
        public SimpleActionTask(string taskDescription, Func<string> action, bool isAttendedTask = false)
        {
            this.TaskDescription = taskDescription;
            this.TaskAction = cancellationToken => { action(); return string.Empty; };
            this.IsAttendedTask = isAttendedTask;
        }

        public override string Execute(CancellationToken cancellationToken)
        {
            return "";
        }

        /// <summary>
        /// Cancels the task.
        /// </summary>
        /// <buildflag>Exclude from Online</buildflag>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new void Cancel()
        {
        }

        ///// <summary>
        ///// Executes the task Asynchronously.
        ///// </summary>
        ///// <buildflag>Exclude from Online</buildflag>
        ///// <param name="cancellationToken"></param>
        ///// <returns>
        ///// CodedTask Execution Result
        ///// </returns>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //public override string ExecuteAsync(CancellationToken cancellationToken)
        //{
        //    return Task.FromResult("");
        //}
    }
}