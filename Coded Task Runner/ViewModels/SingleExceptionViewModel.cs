﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    internal class SingleExceptionViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SingleExceptionViewModel"/> class.
        /// </summary>
        public SingleExceptionViewModel()
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleExceptionViewModel"/> class.
        /// </summary>
        /// <param name="ex">The executable.</param>
        public SingleExceptionViewModel(Exception ex)
        {
            this.Message = ex.Message;
            this.ExceptionType = ex.GetType().ToString();
            try
            {
                this.StackTraceInfo = Regex.Split(ex.StackTrace, " at ", RegexOptions.IgnoreCase)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Select(x => new StackTraceLine(x.Trim()))
                    .ToArray();
            }
            catch
            {
                this.StackTraceInfo = new StackTraceLine[0];
            }
        }

        public string ExceptionType { get; private set; }

        /// <summary>
        /// Gets the message.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the stack trace information.
        /// </summary>
        public StackTraceLine[] StackTraceInfo { get; private set; } 
    }
}