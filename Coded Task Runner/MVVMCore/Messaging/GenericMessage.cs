﻿// **************************************************************************
// <copyright file="GenericMessage.cs" company="GalaSoft Laurent Bugnion">
// Copyright © GalaSoft Laurent Bugnion 2009-2016
// </copyright>
// ****************************************************************************
// <author>Laurent Bugnion</author>
// <email>laurent@galasoft.ch</email>
// <date>13.4.2009</date>
// <project>GalaSoft.MvvmLight.Messaging</project>
// <web>http://www.mvvmlight.net</web>
// <license>
// See license.txt in this project or http://www.galasoft.ch/license_MIT.txt
// </license>
// ****************************************************************************

namespace SpiceLogic.CodedTaskRunner.MVVMCore.Messaging
{
    /// <summary>
    /// Passes a generic value (Content) to a recipient.
    /// </summary>
    /// <typeparam name="T">The type of the Content property.</typeparam>
    ////[ClassInfo(typeof(Messenger))]
    internal class GenericMessage<T> : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the GenericMessage class.
        /// </summary>
        /// <param name="content">The message content.</param>
        protected GenericMessage(T content)
        {
            Content = content;
        }

        /// <summary>
        /// Initializes a new instance of the GenericMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="content">The message content.</param>
        protected GenericMessage(object sender, T content)
            : base(sender)
        {
            Content = content;
        }

        /// <summary>
        /// Gets or sets the message's content.
        /// </summary>
        public T Content { get; private set; }
    }
}