﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using SpiceLogic.CodedTaskRunner.Utils;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// Project Report Type
    /// </summary>
    [DataContract]
    public class ProjectReport
    {
        /// <summary>
        /// Gets or sets the project description.
        /// </summary>
        [DataMember(Order = 0)]
        public string ProjectDescription { get; set; }

        /// <summary>
        /// Gets or sets the task container DLL path.
        /// </summary>
        [DataMember(Order = 1)]
        public string TaskContainerDLLPath { get; set; }

        /// <summary>
        /// Gets or sets the session start date time.
        /// </summary>
        [DataMember(Order = 2)]
        public DateTime? SessionStartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the session end date time.
        /// </summary>
        [DataMember(Order = 3)]
        public DateTime? SessionEndDateTime { get; set; }

        /// <summary>
        /// Gets the total elapsed time information session.
        /// </summary>
        [DataMember(Order = 4)]
        public string TotalElapsedTimeInSession
        {
            get => this.SessionStartDateTime.HasValue && this.SessionEndDateTime.HasValue
                ? CommonUtils.GetElapsedTime(this.SessionStartDateTime.Value, this.SessionEndDateTime.Value)
                : string.Empty;
            // ReSharper disable once ValueParameterNotUsed
            internal set { }
        }

        /// <summary>
        /// Gets or sets the task reports.
        /// </summary>
        [DataMember(Order = 5)]
        public List<CodedTaskReport> TaskReports { get; set; }
    }
}