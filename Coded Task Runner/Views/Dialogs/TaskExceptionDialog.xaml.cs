﻿namespace SpiceLogic.CodedTaskRunner.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for TaskExceptionDialog.xaml
    /// </summary>
    internal partial class TaskExceptionDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskExceptionDialog"/> class.
        /// </summary>
        public TaskExceptionDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
        }
    }
}
