﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using SpiceLogic.CodedTaskRunner.ClientAttributes;

namespace SpiceLogic.CodedTaskRunner.Utils
{
    internal static class CommonUtils
    {
        /// <summary>
        /// Gets the instance from module.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <param name="dllFilePath">The DLL file path.</param>
        /// <returns></returns>
        public static TInterface GetInstanceFromModule<TInterface>(string dllFilePath) where TInterface : class
        {
            try
            {
                Assembly pAssembly = Assembly.LoadFrom(dllFilePath);

                if (pAssembly != null)
                {
                    return pAssembly.GetExportedTypes()
                     .Where(t => t.IsClass && typeof(TInterface).IsAssignableFrom(t))
                     .Select(t => (TInterface)Activator.CreateInstance(t))
                     .FirstOrDefault();
                }
            }
            catch 
            {
            }
            return null;
        }

        /// <summary>
        /// Gets the elapsed time.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="endTime">The end time.</param>
        /// <returns></returns>
        public static string GetElapsedTime(DateTime startTime, DateTime endTime)
        {
            TimeSpan duration = endTime - startTime;
            if (duration.TotalHours >= 1)
                return duration.TotalHours.ToString("0.00") + " hours";
            if (duration.TotalMinutes > 1)
                return duration.TotalMinutes.ToString("0.00") + " minutes";
            if (duration.TotalSeconds > 1)
                return duration.TotalSeconds.ToString("0.00") + " seconds";
            return duration.TotalMilliseconds.ToString("0.00") + " milliseconds";
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <param name="classObj">The class object.</param>
        /// <returns></returns>
        public static string GetDescriptionFromAttribute(this object classObj)
        {
            Type theType = classObj.GetType();
            DescriptionAttribute[] descriptions = (DescriptionAttribute[])
               theType.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptions.Length == 0 ? theType.Name.toSentenceCase() : descriptions[0].Description;
        }

        /// <summary>
        /// Determines whether [is break attribute assigned] [the specified class object].
        /// </summary>
        /// <param name="classObj">The class object.</param>
        /// <returns></returns>
        public static bool IsAttendedTaskAttributeAssigned(this CodedTaskBase classObj)
        {
            Type theType = classObj.GetType();
            AttendedTaskAttribute[] theAttrs = (AttendedTaskAttribute[])theType.GetCustomAttributes(typeof(AttendedTaskAttribute), false);
            return theAttrs.Length > 0;
        }

/*
        /// <summary>
        /// Gets the resource URI by file name.
        /// </summary>
        /// <param name="resourceFileName">Name of the resource file.</param>
        /// <returns></returns>
        public static Uri GetResourceUriByFileName(string resourceFileName)
        {
            return new Uri("pack://application:,,,/SpiceLogicCodedTaskRunner;component/Resources/" + resourceFileName);
        }
*/

        /// <summary>
        /// To the sentence case.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns></returns>
        private static string toSentenceCase(this string str)
        {
            try
            {
                return Regex.Replace(str, "[a-z][A-Z]", m => $"{m.Value[0]} {char.ToLower(m.Value[1])}");
            }
            catch
            {
                return str;
            }
        }
    }
}