﻿using System;
using SpiceLogic.CodedTaskRunner.Views;

namespace SpiceLogic.CodedTaskRunner.MVVMCore.Messaging
{
    internal class ShowDialogWindowMessage : ShowWindowMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShowDialogWindowMessage"/> class.
        /// </summary>
        /// <param name="dlgName">Name of the dialog.</param>
        /// <param name="dataContext">The data context.</param>
        public ShowDialogWindowMessage(DialogFactory.DialogNames dlgName, object dataContext)
            : base(dlgName, dataContext)
        {
        }
  
        /// <summary>
        /// Gets or sets the call back.
        /// </summary>
        public Action<DialogFactory.DialogWindowResult> CallBack { get; set; }
    }
}