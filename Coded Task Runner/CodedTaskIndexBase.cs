﻿using System.Collections.Generic;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner
{
    /// <summary>
    /// This is the base class for Task Index.
    /// </summary>
    public abstract class CodedTaskIndexBase
    {
        /// <summary>
        /// Lists all coded tasks.
        /// </summary>
        /// <returns></returns>
        public abstract IList<CodedTaskBase> ListAllCodedTasks();

        /// <summary>
        /// If this method returns a list of objects (not null) then A combobox will be shown to capture input. The ToString() of that type will be displayed in the combobox.
        /// The combobox will be active only if No task is executed or the user has reset the status of all task.
        /// </summary>
        /// <returns></returns>
        public virtual UserInputSetting GetInputSelectionItems()
        {
            return null;
        }

        /// <summary>
        /// Listses the extra toolbar buttons.
        /// </summary>
        /// <returns></returns>
        public virtual IList<UserToolbarButton> ListsExtraToolbarButtons()
        {
            return null;
        }

        /// <summary>
        /// Called when all tasks command executed.
        /// </summary>
        /// <param name="executionReport">The execution report.</param>
        public virtual async void OnAllTasksCommandExecuted(ProjectReport executionReport)
        {
        }

        /// <summary>
        /// Called when manually executed individual task.
        /// </summary>
        /// <param name="executionReport">The execution report.</param>
        public virtual async void OnManuallyExecutedIndividualTask(CodedTaskReport executionReport)
        {
        }

        /// <summary>
        /// Called when application loaded.
        /// </summary>
        public virtual async void OnApplicationLoaded()
        {
            
        }

        /// <summary>
        /// Called just before application closing.
        /// </summary>
        public virtual async void OnApplicationClosing()
        {
            
        }

        /// <summary>
        /// Called when an input selection is changed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectedItem"></param>
        public virtual async void OnInputSelectionChanged(UserInputItem selectedItem)
        {

        }
    }
}