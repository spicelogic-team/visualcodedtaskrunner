﻿using System;
using System.Windows;
using SpiceLogic.CodedTaskRunner.Views.Dialogs;

namespace SpiceLogic.CodedTaskRunner.Views
{
    /// <summary>
    /// 
    /// </summary>
    internal class DialogFactory
    {
        private static Func<DialogNames, Window> dialogShowingWindowsGetter { get; }

        static DialogFactory()
        {
            dialogShowingWindowsGetter = getShowingWindow;
        }

        public enum DialogNames
        {
            TaskException,
            CompletedTaskResult,
            LogViewer,
            About
        }

        public enum DialogOKCancelResult
        {
            Ok,
            Cancel
        }

        public class DialogWindowResult
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DialogFactory.DialogWindowResult"/> class.
            /// </summary>
            /// <param name="okCancelResult">The ok cancel result.</param>
            public DialogWindowResult(DialogOKCancelResult okCancelResult)
            {
                OkCancelResult = okCancelResult;
            }

            public DialogOKCancelResult OkCancelResult { get; private set; }
            public object DataContext { get; set; }
        }

        /// <summary>
        /// Gets the showing window.
        /// </summary>
        /// <param name="dlgName">Name of the dialog.</param>
        /// <returns></returns>
        private static Window getShowingWindow(DialogNames dlgName)
        {
            Window showingWindow = null;
            switch (dlgName)
            {
                case DialogNames.TaskException:
                    showingWindow = new TaskExceptionDialog();
                    break;
                case DialogNames.CompletedTaskResult:
                    showingWindow = new TaskCompleteResultDialog();
                    break;
                case DialogNames.About:
                    showingWindow = new AboutBox();
                    break;
                case DialogNames.LogViewer:
                    showingWindow = new LogViewerDialog();
                    break;
            }

            return showingWindow;
        }

/*
        /// <summary>
        /// Shows the window.
        /// </summary>
        /// <param name="dlgName">Name of the dialog.</param>
        /// <param name="dataContext">The data context.</param>
        public static void ShowWindow(DialogNames dlgName, object dataContext)
        {
            Window showingWindow = DialogShowingWindowsGetter(dlgName);

            if (showingWindow != null)
            {
                if (dataContext != null)
                    showingWindow.DataContext = dataContext;
                showingWindow.Show();
            }
        }
*/

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="dlgName">Name of the dialog.</param>
        /// <param name="dataContext">The data context.</param>
        /// <returns></returns>
        public static DialogWindowResult ShowDialog(DialogNames dlgName, object dataContext = null)
        {
            Window showingDialog = dialogShowingWindowsGetter(dlgName);

            if (showingDialog != null)
            {
                if (dataContext != null)
                    showingDialog.DataContext = dataContext;

                bool? dlgResult = showingDialog.ShowDialog();

                if (dlgResult.HasValue && dlgResult.Value)
                {
                    object editedData = showingDialog.DataContext;
                    return new DialogWindowResult(DialogOKCancelResult.Ok) {DataContext = editedData};
                }
                
                return new DialogWindowResult(DialogOKCancelResult.Cancel);
            }

            return null;
        }
    }
}
