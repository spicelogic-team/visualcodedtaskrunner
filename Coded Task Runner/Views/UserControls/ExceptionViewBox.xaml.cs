﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SpiceLogic.CodedTaskRunner.ViewModels;

namespace SpiceLogic.CodedTaskRunner.Views.UserControls
{
    /// <summary>
    /// Interaction logic for ExceptionViewBox.xaml
    /// </summary>
    internal partial class ExceptionViewBox
    {
        public ExceptionViewBox()
        {
            InitializeComponent();
        }

        /// <summary>
        /// CTRLs the C copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ListBox lb = (ListBox)(sender);
            StackTraceLine selected = (StackTraceLine) lb.SelectedItem;
            if (selected != null)
                Clipboard.SetText(selected.CallerInfo);
        }

        /// <summary>
        /// CTRLs the C copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Rights the click copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            StackTraceLine selected = (StackTraceLine) mi.DataContext;
            if (selected != null)
                Clipboard.SetText(selected.CallerInfo);
        }

        /// <summary>
        /// Rights the click copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
