﻿using System.ComponentModel;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// Task Run States
    /// </summary>
    public enum TaskRunStates
    {
        /// <summary>
        /// The 'did not start' state
        /// </summary>
        [Description("Did not start")]
        DidNotStart,

        /// <summary>
        /// The running state
        /// </summary>
        Running,

        /// <summary>
        /// The 'ran to completion' state
        /// </summary>
        [Description("Ran to completion")]
        RanToCompletion,

        /// <summary>
        /// The canceled state
        /// </summary>
        Canceled,

        /// <summary>
        /// The faulted state
        /// </summary>
        Faulted
    }
}