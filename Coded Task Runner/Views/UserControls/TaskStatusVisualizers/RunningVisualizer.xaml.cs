﻿using System;
using System.Threading;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner.Views.UserControls.TaskStatusVisualizers
{
    /// <summary>
    /// Interaction logic for RunningVisualizer.xaml
    /// </summary>
    public partial class RunningVisualizer
    {
        private CodedTaskReport _theDataContext;
        private readonly SynchronizationContext _mSynchronizationContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="RunningVisualizer"/> class.
        /// </summary>
        public RunningVisualizer()
        {
            InitializeComponent();
            _mSynchronizationContext = SynchronizationContext.Current;
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            _theDataContext = (CodedTaskReport) this.DataContext;
            _theDataContext.PropertyChanged += theDataContext_PropertyChanged;
        }

        /// <summary>
        /// Handles the PropertyChanged event of the theDataContext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        void theDataContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CodedTaskReport.RunningTaskProgress))
            {
                _mSynchronizationContext.Post(x =>
                {
                    if (_theDataContext.RunningTaskProgress > 0)
                    {
                        progressTask.IsIndeterminate = false;
                        progressTask.Value = Math.Max(0, Math.Min(100, _theDataContext.RunningTaskProgress));
                    }
                    else
                        progressTask.IsIndeterminate = true;

                }, null);
            }
        }

        /// <summary>
        /// Handles the Unloaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            _theDataContext.PropertyChanged -= theDataContext_PropertyChanged;
        }
    }
}
