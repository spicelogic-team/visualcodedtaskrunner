﻿using System;
using System.Globalization;
using System.Windows.Data;
using SpiceLogic.CodedTaskRunner.ViewModels;

namespace SpiceLogic.CodedTaskRunner.ValueConverters
{
    internal class CancellationPendingBasedToolTipConverterForBatchRun : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isCancellationPending = value != null && (bool)value;
            return isCancellationPending ? MainViewModel.BatchTaskCancellationMessage : "Cancel";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}