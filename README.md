# Visual Automated Deployer #

A visual automated deployment solution based on .NET languages like C# or VB.NET. If you are not ready for MS Build based deployment scripting, rather if you like to use C#/VB.NET language for coding your deployment automation, then this is the solution for you.