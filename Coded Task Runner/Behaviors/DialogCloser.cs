﻿using System.Windows;

namespace SpiceLogic.CodedTaskRunner.Behaviors
{
    public static class DialogCloser
    {
        /// <summary>
        /// The dialog result property
        /// </summary>
        public static readonly DependencyProperty DialogResultProperty =
            DependencyProperty.RegisterAttached(
                "DialogResult",
                typeof(bool?),
                typeof(DialogCloser),
                new PropertyMetadata(DialogResultChanged));

        private static void DialogResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Window window = d as Window;
            if (window != null)
                window.DialogResult = e.NewValue as bool?;
        }

        /// <summary>
        /// Sets the dialog result.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">The value.</param>
        public static void SetDialogResult(Window target, bool? value)
        {
            target.SetValue(DialogResultProperty, value);
        }
    }
}