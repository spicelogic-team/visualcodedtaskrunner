﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SpiceLogic.CodedTaskRunner.Behaviors
{
    public static class GridViewExtensions
    {
        /// <summary>
        /// Gets the is content centered.
        /// </summary>
        /// <param name="gridViewColumn">The grid view column.</param>
        /// <returns></returns>
        [Category("Common")]
        [AttachedPropertyBrowsableForType(typeof(GridViewColumn))]
        public static bool GetIsContentCentered(GridViewColumn gridViewColumn)
        {
            return (bool)gridViewColumn.GetValue(IsContentCenteredProperty);
        }

        /// <summary>
        /// Sets the is content centered.
        /// </summary>
        /// <param name="gridViewColumn">The grid view column.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetIsContentCentered(GridViewColumn gridViewColumn, bool value)
        {
            gridViewColumn.SetValue(IsContentCenteredProperty, value);
        }

        /// <summary>
        /// The is content centered property
        /// </summary>
        public static readonly DependencyProperty IsContentCenteredProperty =
            DependencyProperty.RegisterAttached(
                "IsContentCentered",
                typeof(bool), // type
                typeof(GridViewExtensions), // containing type
                new PropertyMetadata(default(bool), OnIsContentCenteredChanged)
                );

        /// <summary>
        /// Called when [is content centered changed].
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnIsContentCenteredChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            OnIsContentCenteredChanged(d, (bool)e.NewValue);
        }

        /// <summary>
        /// Called when [is content centered changed].
        /// </summary>
        /// <param name="gridViewColumn">The grid view column.</param>
        /// <param name="isContentCentered">if set to <c>true</c> [is content centered].</param>
        private static void OnIsContentCenteredChanged(DispatcherObject gridViewColumn, bool isContentCentered)
        {
            if (isContentCentered == false)
                return;
            // must wait a bit otherwise GridViewColumn.DisplayMemberBinding will not yet be initialized, 
            new DispatcherTimer(TimeSpan.FromMilliseconds(100), DispatcherPriority.Normal, OnColumnLoaded,
                gridViewColumn.Dispatcher)
            {
                Tag = gridViewColumn
            }.Start();
        }

        /// <summary>
        /// Called when [column loaded].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.Exception">Only allowed with DisplayMemberBinding.</exception>
        private static void OnColumnLoaded(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();

            GridViewColumn gridViewColumn = (GridViewColumn)timer.Tag;
            if (gridViewColumn.DisplayMemberBinding == null)
                throw new Exception("Only allowed with DisplayMemberBinding.");

            FrameworkElementFactory textBlockFactory = new FrameworkElementFactory(typeof(TextBlock));
            textBlockFactory.SetBinding(TextBlock.TextProperty, gridViewColumn.DisplayMemberBinding);
            textBlockFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);
            DataTemplate cellTemplate = new DataTemplate { VisualTree = textBlockFactory };
            gridViewColumn.DisplayMemberBinding = null; // must null, otherwise CellTemplate won't be recognized
            gridViewColumn.CellTemplate = cellTemplate;
        }
    }
}
