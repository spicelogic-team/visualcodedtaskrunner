﻿using System;
using System.Text;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner.Views.UserControls.TaskStatusVisualizers
{
    /// <summary>
    /// Interaction logic for RanToCompletionVisualizer.xaml
    /// </summary>
    public partial class RanToCompletionVisualizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RanToCompletionVisualizer"/> class.
        /// </summary>
        public RanToCompletionVisualizer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            CodedTaskReport theDataContext = (CodedTaskReport)this.DataContext;

            if (theDataContext.IsManualInstruction)
            {
                btnCompletedTask.Visibility = System.Windows.Visibility.Collapsed;
                imgForManualInstruction.Visibility = System.Windows.Visibility.Visible;
                imgForManualInstruction.ToolTip =
                    $"Instruction Followed at {theDataContext.StartTime ?? DateTime.MinValue}";
            }
            else
            {
                btnCompletedTask.Visibility = System.Windows.Visibility.Visible;
                imgForManualInstruction.Visibility = System.Windows.Visibility.Collapsed;

                StringBuilder tooltipBuilder = new StringBuilder();
                tooltipBuilder.AppendLine($"Ran to completion, ({theDataContext.ElapsedTime})");
                tooltipBuilder.AppendLine("Click to view details..");
                btnCompletedTask.ToolTip = tooltipBuilder.ToString().Trim();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCompletedTask control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCompletedTask_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogFactory.ShowDialog(DialogFactory.DialogNames.CompletedTaskResult, this.DataContext); // DataContext is CodedTaskReport
        }
    }
}
