﻿using SpiceLogic.CodedTaskRunner.Views;

namespace SpiceLogic.CodedTaskRunner.MVVMCore.Messaging
{
    internal class ShowWindowMessage : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShowDialogWindowMessage" /> class.
        /// </summary>
        /// <param name="dlgName">Name of the dialog.</param>
        /// <param name="dataContext">The data context.</param>
        protected ShowWindowMessage(DialogFactory.DialogNames dlgName, object dataContext)
        {
            this.DialogName = dlgName;
            this.WindowDataContext = dataContext;
        }

        /// <summary>
        /// Gets the name of the dialog.
        /// </summary>
        public DialogFactory.DialogNames DialogName { get; }

        /// <summary>
        /// Gets the window data context.
        /// </summary>
        public object WindowDataContext { get; }
    }
}