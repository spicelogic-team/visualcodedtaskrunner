﻿using System;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// User Toolbar Button type
    /// </summary>
    public class UserToolbarButton
    {
        /// <summary>
        /// Gets or sets the button text.
        /// </summary>
        /// <value>
        /// The button text.
        /// </value>
        public string ButtonText { get; set; }

        /// <summary>
        /// Gets or sets the toolbar image source.
        /// </summary>
        /// <value>
        /// The toolbar image source.
        /// </value>
        public Uri ToolbarImageSource { get; set; }

        /// <summary>
        /// Gets or sets the image dimension.
        /// </summary>
        /// <value>
        /// The image dimension.
        /// </value>
        public int ImageDimension { get; set; }

        /// <summary>
        /// Gets or sets the tooltip.
        /// </summary>
        /// <value>
        /// The tooltip.
        /// </value>
        public string Tooltip { get; set; }

        /// <summary>
        /// Gets or sets the click action.
        /// </summary>
        /// <value>
        /// The click action.
        /// </value>
        public Action ClickAction { get; set; }
    }
}