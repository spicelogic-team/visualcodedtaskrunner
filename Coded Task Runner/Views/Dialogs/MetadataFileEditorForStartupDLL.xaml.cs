﻿using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace SpiceLogic.CodedTaskRunner.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for MetadataFileEditorForStartupDLL.xaml
    /// </summary>
    public partial class MetadataFileEditorForStartupDLL
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataFileEditorForStartupDLL"/> class.
        /// </summary>
        /// <param name="preLoadedFileName">Name of the pre loaded file.</param>
        public MetadataFileEditorForStartupDLL(string preLoadedFileName)
        {
            InitializeComponent();
            txtDLLFileName.Text = preLoadedFileName;
        }

        /// <summary>
        /// Gets the name of the DLL file.
        /// </summary>
        /// <value>
        /// The name of the DLL file.
        /// </value>
        public string DLLFileName
        {
            get
            {
                string theFileName = Path.GetFileName(txtDLLFileName.Text);
                return !string.IsNullOrEmpty(theFileName) ? theFileName : txtDLLFileName.Text.Trim();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDLLFileName.Text))
            {
                MessageBox.Show("Please provide the DLL file name");
                txtDLLFileName.Focus();
                return;
            }

            this.DialogResult = true;
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Handles the Click event of the btnSelectFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDlg = new OpenFileDialog
            {
                Filter = "DLL Files (*.dll)|*.dll",
                FilterIndex = 0,
                InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
            };

            if (fileDlg.ShowDialog() == true)
                txtDLLFileName.Text = Path.GetFileName(fileDlg.FileName);
        }
    }
}
