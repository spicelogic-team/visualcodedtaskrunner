﻿using System.IO;
using System.Windows;
using Microsoft.Win32;
using SpiceLogic.CodedTaskRunner.Models;
using SpiceLogic.CodedTaskRunner.MVVMCore;
using SpiceLogic.CodedTaskRunner.MVVMCore.Command;
using SpiceLogic.CodedTaskRunner.MVVMCore.Messaging;
using SpiceLogic.CodedTaskRunner.Utils;
using SpiceLogic.CodedTaskRunner.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    internal class MainViewModel : ViewModelBase, IDisposable
    {
        /// <summary>
        /// The _cancallation token source
        /// </summary>
        private CancellationTokenSource _cancallationTokenSrc;

        private readonly Progress<Tuple<int, string>> _progressObj;

        public const string SelectedTaskCancellationMessage =
            "Your cancel request has been transmitted to the individual task code. \nBased on your coded task, the current task execution may get canceled.";

        public const string BatchTaskCancellationMessage =
            "Your cancel request has been received. Even if the current ongoing task \ncannot be forcefully terminated, the next task in the list will not be executed.";

        private bool _searchingLog;
        private int _progress;
        private string _dllFileChangeLinkText = "Select DLL File that contains your tasks";
        private string _statusBarMessage;
        private string _receivedDllFilePath;
        private string _runAllSequentialButtonText = "Run All Tasks";
        private string _runAllSequentialButtonTooltip = "Run all tasks sequentially";
        private string _projectDescription;

        private bool _isProjectDirty;
        private bool _isCancelationPending;
        private bool _isSingleTaskRunning;

        private DecoratedCodedTask _selectedIndividualTask;
        private List<DecoratedCodedTask> _allDecoratedTasks;
        private CodedTaskIndexBase _loadedTaskIndexFromAssembly;
        private ObservableCollection<DecoratedCodedTask> _allTasks;
        private TaskRunStates _overallProjectStatus = TaskRunStates.DidNotStart;
        private IList<DecoratedUserToolbarButton> _userToolbarButtons = new List<DecoratedUserToolbarButton>();
        private ObservableCollection<ActivityLog> _logs;
        private ObservableCollection<ActivityLog> _logsSearchResult = new ObservableCollection<ActivityLog>();
        private string _searchText;
        private bool _searchResetButtonVisible;
        private string _searchLogText;
        private bool _searchLogResetButtonVisible;

        private RelayCommand _configureVsProjectForDebugCommand;
        private RelayCommand _runAllTasksCommand;
        private RelayCommand<int> _runIndividualTaskCommand;
        private RelayCommand<int> _cancelSelectedTaskCommand;
        private RelayCommand _cancelAllTaskRunCommand;
        private RelayCommand _resetAllTaskStatusCommand;
        private RelayCommand _exportTaskResultToXMLCommand;
        private RelayCommand _changePluginDllPathCommand;
        private RelayCommand _showLogWindowCommand;
        private RelayCommand _saveLogCommand;
        private RelayCommand _clearLogsCommand;
        private RelayCommand _searchCommand;
        private RelayCommand _searchResetCommand;
        private RelayCommand<int> _markSingleTaskAsExecutedCommand;
        private RelayCommand _searchLogCommand;
        private RelayCommand _searchLogResetCommand;
        private ObservableCollection<UserInputItem> _inputSelectItems;
        private UserInputItem _selectedInputItem;
        private string _inputSelectionLabel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            _logs = new ObservableCollection<ActivityLog>(new List<ActivityLog>());
            addToLog(ActivityLog.LogTypes.Event, "Applicatin Started");

            this._progressObj = new Progress<Tuple<int, string>>();
            this._progressObj.ProgressChanged += progressObjProgressObjChanged;
            this.StatusBarMessage = $"Welcome {Environment.UserName}";
            addToLog(ActivityLog.LogTypes.Info, this.StatusBarMessage);
        }

        /// <summary>
        /// Adds to log.
        /// </summary>
        /// <param name="logType">Type of the log.</param>
        /// <param name="performedAction">The performed action.</param>
        private void addToLog(ActivityLog.LogTypes logType, string performedAction)
        {
            Application.Current.Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.Normal, (Action) delegate
                {
                    if (string.IsNullOrWhiteSpace(performedAction))
                        return;

                    int nextId = _logs.Count == 0 ? 1 : _logs.Max(x => x.ID) + 1;

                    _logs.Add(new ActivityLog {ID = nextId, Message = performedAction.Trim(), LogType = logType});
                });
        }

        /// <summary>
        /// Progress_s the _progressObj changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The decimal.</param>
        private void progressObjProgressObjChanged(object sender, Tuple<int, string> e)
        {
            this.Progress = e.Item1;
            this.StatusBarMessage = e.Item2;
        }

        /// <summary>
        /// Gets or sets the logs.
        /// </summary>
        public ObservableCollection<ActivityLog> Logs
        {
            get => _searchingLog ? _logsSearchResult : _logs;
            set
            {
                if (_searchingLog)
                {
                    if (!Equals(_logsSearchResult, value))
                    {
                        _logsSearchResult = value;
                        OnPropertyChanged();
                    }
                }
                else
                {
                    if (!Equals(_logs, value))
                    {
                        _logs = value;
                        OnPropertyChanged();
                    }
                }
            }
        }

        public string InputSelectionLabel
        {
            get => _inputSelectionLabel;
            set
            {
                if (_inputSelectionLabel != value)
                {
                    _inputSelectionLabel = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<UserInputItem> InputSelectItems
        {
            get => _inputSelectItems;
            set
            {
                if (!Equals(_inputSelectItems, value))
                {
                    _inputSelectItems = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(ShouldVisibleSelectionInput));
                }
            }
        }

        public UserInputItem SelectedInputItem
        {
            get => _selectedInputItem;
            set
            {
                if (!Equals(_selectedInputItem, value))
                {
                    _selectedInputItem = value;
                    OnPropertyChanged();
                    addToLog(ActivityLog.LogTypes.Event, "Selected Input changed to -> " + value.DisplayText);
                    LoadedTaskIndexFromAssembly.OnInputSelectionChanged(value);
                }
            }
        }

        public bool ShouldEnableSelectionInput => OverallProjectStatus == TaskRunStates.DidNotStart && !IsProjectDirty;

        public bool ShouldVisibleSelectionInput => InputSelectItems != null && InputSelectItems.Count > 0;

        /// <summary>
        /// Gets or sets the project description.
        /// </summary>
        public string ProjectDescription
        {
            get => _projectDescription;
            set
            {
                if (_projectDescription != value)
                {
                    _projectDescription = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the overall project status.
        /// </summary>
        public TaskRunStates OverallProjectStatus
        {
            get => _overallProjectStatus;
            set
            {
                if (_overallProjectStatus != value)
                {
                    _overallProjectStatus = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(ShouldEnableSelectionInput));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the project's any of the task has run so far, even with success, cancellation or exception.
        /// </summary>
        public bool IsProjectDirty
        {
            get => _isProjectDirty;
            set
            {
                if (_isProjectDirty != value)
                {
                    _isProjectDirty = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(ShouldEnableSelectionInput));
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [is cancelation pending].
        /// </summary>
        public bool IsCancelationPending
        {
            get => _isCancelationPending;
            set
            {
                if (_isCancelationPending != value)
                {
                    _isCancelationPending = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the run all sequential button text.
        /// </summary>
        public string RunAllSequentialButtonText
        {
            get => _runAllSequentialButtonText;
            set
            {
                if (_runAllSequentialButtonText != value)
                {
                    _runAllSequentialButtonText = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the run all sequential button tooltip.
        /// </summary>
        public string RunAllSequentialButtonTooltip
        {
            get => _runAllSequentialButtonTooltip;
            set
            {
                if (_runAllSequentialButtonTooltip != value)
                {
                    _runAllSequentialButtonTooltip = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the DLL file change link text.
        /// </summary>
        public string DLLFileChangeLinkText
        {
            get => _dllFileChangeLinkText;
            set
            {
                if (_dllFileChangeLinkText != value)
                {
                    _dllFileChangeLinkText = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the loaded DLL file path.
        /// </summary>
        public string ReceivedDLLFilePath
        {
            get => _receivedDllFilePath;
            set
            {
                if (_receivedDllFilePath != value)
                {
                    if (ApplicationStates.AlreadyLoadedPluginFromAssembly && !string.IsNullOrWhiteSpace(value))
                    {
                        Messenger.Default.Send(new NotificationMessage(value), MessageNames.RestartApplicationForNewDLL);
                        return;
                    }

                    _receivedDllFilePath = value;

                    addToLog(ActivityLog.LogTypes.Event, "DLL File Path Set : " + value);

                    OnPropertyChanged();

                    this.DLLFileChangeLinkText = "Change";

                    LoadedTaskIndexFromAssembly = CommonUtils.GetInstanceFromModule<CodedTaskIndexBase>(value);

                    if (LoadedTaskIndexFromAssembly != null)
                        ApplicationStates.AlreadyLoadedPluginFromAssembly = true;
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(value))
                        {
                            string warningMessage =
                                $"The file {value} is not a valid Plugin DLL. No instance of CodedTaskIndexBase found.";
                            addToLog(ActivityLog.LogTypes.Warning, warningMessage);
                            Messenger.Default.Send(new ShowMessageBoxMessage(warningMessage)
                            {
                                Title = "Invalid Plugin DLL"
                            });
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Gets or sets the loaded task index from assembly.
        /// </summary>
        public CodedTaskIndexBase LoadedTaskIndexFromAssembly
        {
            get => _loadedTaskIndexFromAssembly;
            set
            {
                if (!Equals(_loadedTaskIndexFromAssembly, value))
                {
                    _loadedTaskIndexFromAssembly = value;

                    addToLog(ActivityLog.LogTypes.Event, "Successfully set Task Index from the loaded DLL");

                    OnPropertyChanged();

                    UserInputSetting userInputSetting = LoadedTaskIndexFromAssembly.GetInputSelectionItems();

                    if (userInputSetting != null && userInputSetting.SelectionItems?.Count > 0)
                    {
                        InputSelectionLabel = userInputSetting.SelectionInputLabel;
                        InputSelectItems = new ObservableCollection<UserInputItem>(userInputSetting.SelectionItems);
                        addToLog(ActivityLog.LogTypes.Info,
                            $"Total {userInputSetting.SelectionItems.Count} input type loaded for the selection.");

                        bool preSelectionItemFound = false;

                        if (!string.IsNullOrEmpty(userInputSetting.SelectedItemDisplayText))
                        {
                            foreach (UserInputItem anItem in InputSelectItems)
                            {
                                if(string.Equals(anItem.DisplayText, userInputSetting.SelectedItemDisplayText, StringComparison.OrdinalIgnoreCase))
                                {
                                    SelectedInputItem = anItem;
                                    preSelectionItemFound = true;
                                    break;
                                }
                            }
                        }
                         
                        if(!preSelectionItemFound)
                            SelectedInputItem = InputSelectItems[0];
                    }

                    _allDecoratedTasks = new List<DecoratedCodedTask>();
                    if (_loadedTaskIndexFromAssembly != null)
                    {
                        this.ProjectDescription = value.GetDescriptionFromAttribute();
                        _allDecoratedTasks.AddRange(_loadedTaskIndexFromAssembly.ListAllCodedTasks()
                            .Select((anUserTask, i) =>
                            {
                                Type theType = anUserTask.GetType();
                                bool isManualInstructionTask = anUserTask is ManualInstructionTask;
                                ManualInstructionTask theManualTask = null;
                                if (isManualInstructionTask)
                                    theManualTask = (ManualInstructionTask) anUserTask;
                                SimpleActionTask theSimpleActionTask = null;
                                bool isSimpleActionTask = anUserTask is SimpleActionTask;
                                if (isSimpleActionTask)
                                    theSimpleActionTask = (SimpleActionTask) anUserTask;
                                return new DecoratedCodedTask
                                {
                                    CodedTask = anUserTask,
                                    TaskInfo = new CodedTaskReport
                                    {
                                        Name = theType.Name,
                                        FullName = theType.FullName,
                                        Description =
                                            isSimpleActionTask
                                                ? theSimpleActionTask.TaskDescription
                                                : (isManualInstructionTask
                                                    ? theManualTask.Instruction
                                                    : anUserTask.GetDescriptionFromAttribute()),
                                        Status = TaskRunStates.DidNotStart,
                                        IsAttendedTask =
                                            isSimpleActionTask
                                                ? theSimpleActionTask.IsAttendedTask
                                                : anUserTask.IsAttendedTaskAttributeAssigned(),
                                        IsManualInstruction = isManualInstructionTask,
                                        Sequence = i + 1
                                    }
                                };
                            }));

                        IList<UserToolbarButton> theUserButtons =
                            _loadedTaskIndexFromAssembly.ListsExtraToolbarButtons();
                        if (theUserButtons != null)
                            this.UserToolbarButtons =
                                theUserButtons.Select(x => new DecoratedUserToolbarButton(x)).ToList();
                    }

                    if (_allDecoratedTasks != null)
                    {
                        foreach (DecoratedCodedTask aTask in _allDecoratedTasks)
                        {
                            aTask.CodedTask.ProgressChanged += CodedTask_ProgressChanged;
                            aTask.CodedTask.StatusMessageUpdated += CodedTask_StatusMessageUpdated;
                        }
                    }

                    this.SearchText = string.Empty;
                    this.AllTasks = new ObservableCollection<DecoratedCodedTask>(_allDecoratedTasks);

                    Messenger.Default.Send(new NotificationMessage(null), MessageNames.FocusAndSelectFirstTask);
                }
            }
        }

        /// <summary>
        /// Handles the StatusMessageUpdated event of the CodedTask control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="StatusMessageUpdateEventArgs"/> instance containing the event data.</param>
        private void CodedTask_StatusMessageUpdated(object sender, StatusMessageUpdateEventArgs e)
        {
            this.StatusBarMessage = e.StatusMessage;
        }

        /// <summary>
        /// Handles the ProgressChanged event of the CodedTask control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void CodedTask_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.SelectedIndividualTask.TaskInfo.RunningTaskProgress = e.ProgressPercentage;
        }

        /// <summary>
        /// Gets or sets the user toolbar buttons.
        /// </summary>
        public IList<DecoratedUserToolbarButton> UserToolbarButtons
        {
            get => _userToolbarButtons;
            set
            {
                if (!Equals(_userToolbarButtons, value))
                {
                    _userToolbarButtons = value;
                    OnPropertyChanged();
                }
            }
        }

        public string SearchText
        {
            get => _searchText;
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    OnPropertyChanged();
                    this.SearchResetButtonVisible = !string.IsNullOrWhiteSpace(value);
                }
            }
        }

        public string SearchLogText
        {
            get => _searchLogText;
            set
            {
                if (_searchLogText != value)
                {
                    _searchLogText = value;
                    OnPropertyChanged();
                    this.SearchLogResetButtonVisible = !string.IsNullOrWhiteSpace(value);
                }
            }
        }

        public bool SearchLogResetButtonVisible
        {
            get => _searchLogResetButtonVisible;
            set
            {
                if (_searchLogResetButtonVisible != value)
                {
                    _searchLogResetButtonVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool SearchResetButtonVisible
        {
            get => _searchResetButtonVisible;
            set
            {
                if (_searchResetButtonVisible != value)
                {
                    _searchResetButtonVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets all tasks.
        /// </summary>
        public ObservableCollection<DecoratedCodedTask> AllTasks
        {
            get => _allTasks;
            set
            {
                if (!Equals(_allTasks, value))
                {
                    _allTasks = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the work status.
        /// </summary>
        public string StatusBarMessage
        {
            get => _statusBarMessage;
            set
            {
                if (_statusBarMessage != value)
                {
                    _statusBarMessage = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the _progressObj.
        /// </summary>
        public int Progress
        {
            get => _progress;
            set
            {
                if (_progress != value)
                {
                    _progress = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [is single task running].
        /// </summary>
        public bool IsSingleTaskRunning
        {
            get => _isSingleTaskRunning;
            set
            {
                if (_isSingleTaskRunning != value)
                {
                    _isSingleTaskRunning = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the selected individual task.
        /// </summary>
        public DecoratedCodedTask SelectedIndividualTask
        {
            get => _selectedIndividualTask;
            set
            {
                if (!Equals(_selectedIndividualTask, value))
                {
                    _selectedIndividualTask = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the total logs.
        /// </summary>
        public int TotalLogs => this.Logs.Count;

        /// <summary>
        /// Gets the search log command.
        /// </summary>
        public RelayCommand SearchLogCommand
        {
            get
            {
                return _searchLogCommand ?? (_searchLogCommand = new RelayCommand(() =>
                {
                    if (_logs != null && _logs.Count > 0)
                    {
                        _searchingLog = true;

                        this.Logs = string.IsNullOrWhiteSpace(this.SearchLogText)
                            ? new ObservableCollection<ActivityLog>(_logs)
                            : new ObservableCollection<ActivityLog>(_logs.Where(x => (x.Message != null && x.Message.ToUpper().Contains(this.SearchLogText.ToUpper()))));

                        OnPropertyChanged(nameof(TotalLogs));
                    }
                }));
            }
        }

        /// <summary>
        /// Gets or sets the search log reset command.
        /// </summary>
        public RelayCommand SearchLogResetCommand
        {
            get
            {
                return _searchLogResetCommand ?? (_searchLogResetCommand = new RelayCommand(() =>
                {
                    this.SearchLogText = string.Empty;
                    _searchingLog = false;
                    if (_logs != null && _logs.Count > 0)
                    {
                        this.Logs = new ObservableCollection<ActivityLog>(_logs);
                        OnPropertyChanged(nameof(TotalLogs));
                    }
                }));
            }
        }

        /// <summary>
        /// Gets the search command.
        /// </summary>
        public RelayCommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new RelayCommand(() =>
                {
                    if (this.AllTasks != null && this.AllTasks.Count > 0)
                    {
                        this.AllTasks = string.IsNullOrWhiteSpace(this.SearchText)
                            ? new ObservableCollection<DecoratedCodedTask>(_allDecoratedTasks)
                            : new ObservableCollection<DecoratedCodedTask>(
                                _allDecoratedTasks.Where(
                                    x =>
                                        x.TaskInfo.Description != null &&
                                        x.TaskInfo.Description.ToUpper().Contains(this.SearchText.ToUpper())));
                        this.StatusBarMessage = $"Total {this.AllTasks.Count} tasks found";
                    }
                }));
            }
        }

        /// <summary>
        /// Gets the search reset command.
        /// </summary>
        public RelayCommand SearchResetCommand
        {
            get
            {
                return _searchResetCommand ?? (_searchResetCommand = new RelayCommand(() =>
                {
                    this.SearchText = string.Empty;
                    if (_allDecoratedTasks != null && _allDecoratedTasks.Count > 0)
                    {
                        this.AllTasks = new ObservableCollection<DecoratedCodedTask>(_allDecoratedTasks);
                        this.StatusBarMessage = $"Total {this.AllTasks.Count} tasks found";
                    }
                }));
            }
        }

        /// <summary>
        /// Gets the export task result automatic xml command.
        /// </summary>
        public RelayCommand ExportTaskResultToXMLCommand
        {
            get
            {
                return _exportTaskResultToXMLCommand ?? (_exportTaskResultToXMLCommand
                    = new RelayCommand(() => Messenger.Default.Send(new SaveFileMessage
                    {
                        Dialog = new SaveFileDialog
                        {
                            Filter = "Xml Files (*.xml)|*.xml|All Files (*.*)|*.*",
                            FilterIndex = 0,
                            OverwritePrompt = true,
                            Title = "Select a location to save the xml report.",
                            DefaultExt = ".xml",
                            AddExtension = true
                        },
                        CallBackWithSelectedFilePath = selectedFilePath =>
                        {
                            if (selectedFilePath != null)
                            {
                                List<CodedTaskReport> theTaskInfoCollection =
                                    this.AllTasks.Select(x => x.TaskInfo).ToList();
                                ProjectReport theExportModel = new ProjectReport
                                {
                                    TaskContainerDLLPath = this.ReceivedDLLFilePath,
                                    ProjectDescription = this.ProjectDescription,
                                    TaskReports = theTaskInfoCollection,
                                    SessionStartDateTime = theTaskInfoCollection.Min(x => x.StartTime),
                                    SessionEndDateTime = theTaskInfoCollection.Max(x => x.EndTime),
                                };

                                try
                                {
                                    theExportModel.Serialize(selectedFilePath);
                                    addToLog(ActivityLog.LogTypes.Event,
                                        "Successfully exported the task results at " + selectedFilePath);

                                    Messenger.Default.Send(
                                        new ShowMessageBoxMessage("Successfully exported the task results!")
                                        {
                                            Title = "Success!"
                                        });
                                }
                                catch (Exception err)
                                {
                                    addToLog(ActivityLog.LogTypes.Exception,
                                        $"Error exporting task results! {err.Message}");

                                    Messenger.Default.Send(new ShowMessageBoxMessage(err.Message)
                                    {
                                        Title = "Error!"
                                    });
                                }
                            }
                        }
                    })));
            }
        }

        /// <summary>
        /// Shows the exception dialog.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="windowTitle">The window title.</param>
        public void ShowExceptionDialog(Exception ex, string windowTitle)
        {
            Messenger.Default.Send(new ShowDialogWindowMessage(DialogFactory.DialogNames.TaskException,
                new ExceptionViewModel(ex, windowTitle)));
        }

        /// <summary>
        /// Gets the mark single task as executed command.
        /// </summary>
        public RelayCommand<int> MarkSingleTaskAsExecutedCommand
        {
            get
            {
                return _markSingleTaskAsExecutedCommand ?? (_markSingleTaskAsExecutedCommand = new RelayCommand<int>(
                    taskSequence =>
                    {
                        this.SelectedIndividualTask = this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);

                        if (this.SelectedIndividualTask == null)
                            return;

                        this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.RanToCompletion;

                        string statusMessage;
                        if (this.SelectedIndividualTask.TaskInfo.IsManualInstruction)
                        {
                            statusMessage =
                                $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} [Manual Instruction] Followed : {this.SelectedIndividualTask.TaskInfo.Description} ....";
                            this.StatusBarMessage = statusMessage;
                            addToLog(ActivityLog.LogTypes.Event, statusMessage);
                        }
                        else
                        {
                             statusMessage =
                                 $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} Marked as Executed : {this.SelectedIndividualTask.TaskInfo.Description} ....";
                            this.StatusBarMessage = statusMessage;
                            addToLog(ActivityLog.LogTypes.Event, statusMessage);
                        }

                        IsProjectDirty = true;
                    }));
            }
        }

        /// <summary>
        /// Gets the run individual task command.
        /// </summary>
        public RelayCommand<int> RunIndividualTaskCommand
        {
            get
            {
                return _runIndividualTaskCommand ?? (_runIndividualTaskCommand
                    = new RelayCommand<int>(async taskSequence =>
                    {
                        this.SelectedIndividualTask =
                            this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);

                        if (this.SelectedIndividualTask == null)
                            return;

                        _cancallationTokenSrc = new CancellationTokenSource();

                        try
                        {
                            this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Running;
                            this.IsSingleTaskRunning = true;

                            SimpleActionTask theSimpleActionTask =
                                this.SelectedIndividualTask.CodedTask as SimpleActionTask;

                            string taskType = theSimpleActionTask != null ? " (Simple Action)" : "";
                            string statusMessage =
                                $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} Started Individually{taskType}: {this.SelectedIndividualTask.TaskInfo.Description} ....";
                            this.StatusBarMessage = statusMessage;
                            addToLog(ActivityLog.LogTypes.Event, statusMessage);

                            this.SelectedIndividualTask.TaskInfo.StartTime = DateTime.Now;

                            await Task.Run(() =>
                            {
                                this.SelectedIndividualTask.TaskInfo.ReturnedResponse = theSimpleActionTask != null 
                                    ? theSimpleActionTask.TaskAction.Invoke(_cancallationTokenSrc.Token) 
                                    : this.SelectedIndividualTask.CodedTask.Execute(_cancallationTokenSrc.Token);
                            });

                            addToLog(ActivityLog.LogTypes.TaskExecutionResult,
                                this.SelectedIndividualTask.TaskInfo.ReturnedResponse);

                            this.SelectedIndividualTask = this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);
                            if (this.SelectedIndividualTask == null)
                                return;

                            this.SelectedIndividualTask.TaskInfo.EndTime = DateTime.Now;
                            this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.RanToCompletion;
                          
                            statusMessage = $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} Completed.";
                            this.StatusBarMessage = statusMessage;
                            addToLog(ActivityLog.LogTypes.Event, statusMessage);
                        }
                        catch (OperationCanceledException)
                        {
                            this.SelectedIndividualTask = this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);
                            if (this.SelectedIndividualTask == null)
                                return;

                            this.SelectedIndividualTask.TaskInfo.EndTime = DateTime.Now;
                            this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Canceled;
                            this.StatusBarMessage =
                                $"Cancelled the CodedTask # {this.SelectedIndividualTask.TaskInfo.Sequence}";
                            addToLog(ActivityLog.LogTypes.Warning, this.StatusBarMessage);
                        }
                        catch (Exception err)
                        {
                            this.SelectedIndividualTask =
                                this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);
                            if (this.SelectedIndividualTask == null)
                                return;

                            this.SelectedIndividualTask.TaskInfo.EndTime = DateTime.Now;
                            this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Faulted;
                            this.SelectedIndividualTask.TaskInfo.ThrownException = err;
                            string exceptionTitle =
                                string.Format(err.GetType().ToString() + " occured in Task # {0} : {1}",
                                    this.SelectedIndividualTask.TaskInfo.Sequence,
                                    this.SelectedIndividualTask.TaskInfo.Description);
                            addToLog(ActivityLog.LogTypes.Exception,
                                string.Format("{0}{1}{2}{1}{1}Stack Trace : {1}---------------------{1}{3}",
                                    exceptionTitle, Environment.NewLine, err.Message, err.StackTrace));
                            ShowExceptionDialog(SelectedIndividualTask.TaskInfo.ThrownException, exceptionTitle);
                        }
                        finally
                        {
                            this.IsCancelationPending = false;
                            this.IsProjectDirty = true;
                            this.IsSingleTaskRunning = false;
                        }

                        try
                        {
                            this.LoadedTaskIndexFromAssembly.OnManuallyExecutedIndividualTask(this.SelectedIndividualTask.TaskInfo);
                            addToLog(ActivityLog.LogTypes.Event, "Executed OnManuallyExecutedIndividualTask() overridable action");
                        }
                        catch (Exception err)
                        {
                            const string errTitle = "Error occured in your OnManuallyExecutedIndividualTask() method.";
                            addToLog(ActivityLog.LogTypes.Exception, errTitle);
                            ShowExceptionDialog(err, errTitle);
                        }
                    }));
            }
        }

        /// <summary>
        /// Gets the run all tasks sequentially command.
        /// </summary>
        public RelayCommand RunAllTasksCommand
        {
            get
            {
                return _runAllTasksCommand ?? (_runAllTasksCommand = new RelayCommand(async () =>
                {
                    if (LoadedTaskIndexFromAssembly == null)
                    {
                        Messenger.Default.Send(
                            new ShowMessageBoxMessage(
                                "You did not load any Plugin Assembly yet which contains your tasks.")
                            {
                                Title = "No Plugin Loaded!"
                            });
                        return;
                    }

                    if (AllTasks == null || AllTasks.Count == 0)
                    {
                        Messenger.Default.Send(
                            new ShowMessageBoxMessage(
                                "You did not load any CodedTask to execute in your plugin dll.")
                            {
                                Title = "No Tasks registered!"
                            });
                        return;
                    }

                    _cancallationTokenSrc = new CancellationTokenSource();

                    bool exceptionThrown = false;
                    bool statusMessageSet = false;

                    List<CodedTaskReport> theResults = await Task.Run(async () =>
                    {
                        List<CodedTaskReport> allTaskResults = new List<CodedTaskReport>();

                        if (AllTasks.All(x => x.TaskInfo.Status != TaskRunStates.DidNotStart))
                        {
                            this.IsProjectDirty = false;

                            foreach (DecoratedCodedTask aTask in this.AllTasks)
                            {
                                aTask.TaskInfo.Status = TaskRunStates.DidNotStart;
                                aTask.TaskInfo.ReturnedResponse = null;
                                aTask.TaskInfo.StartTime = null;
                                aTask.TaskInfo.EndTime = null;
                                aTask.TaskInfo.ThrownException = null;
                            }
                        }

                        this.OverallProjectStatus = TaskRunStates.Running;

                        for (int i = 0; i < AllTasks.Count; i++)
                        {
                            try
                            {
                                this.SelectedIndividualTask = AllTasks[i];

                                Messenger.Default.Send(
                                    new NotificationMessage<DecoratedCodedTask>(this.SelectedIndividualTask, ""),
                                    MessageNames.ScrollListToView);

                                if (this.SelectedIndividualTask.TaskInfo.Status != TaskRunStates.DidNotStart)
                                    continue;

                                if (_cancallationTokenSrc.Token.IsCancellationRequested ||
                                    this.SelectedIndividualTask.TaskInfo.IsAttendedTask ||
                                    this.SelectedIndividualTask.TaskInfo.IsManualInstruction)
                                {
                                    this.RunAllSequentialButtonText = "Resume Sequential Run";
                                    this.RunAllSequentialButtonTooltip =
                                        "Resume executing rest of the tasks that did not run.";
                                    this.OverallProjectStatus = TaskRunStates.Canceled;

                                    if (_cancallationTokenSrc.Token.IsCancellationRequested)
                                        this.StatusBarMessage = "As cancellation requested, batch running stopped.";
                                    else if (this.SelectedIndividualTask.TaskInfo.IsAttendedTask)
                                        this.StatusBarMessage =
                                            $"As the CodedTask # {this.SelectedIndividualTask.TaskInfo.Sequence} is an attended task, batch running stopped.";
                                    else if (this.SelectedIndividualTask.TaskInfo.IsManualInstruction)
                                        this.StatusBarMessage =
                                            $"As the Task # {this.SelectedIndividualTask.TaskInfo.Sequence} is a Manual Instruction, batch running stopped.";

                                    addToLog(ActivityLog.LogTypes.Event, this.StatusBarMessage);

                                    statusMessageSet = true;
                                    break;
                                }

                                this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Running;

                                this.SelectedIndividualTask.TaskInfo.StartTime = DateTime.Now;

                                SimpleActionTask theSimpleActionTask =
                                    this.SelectedIndividualTask.CodedTask as SimpleActionTask;
                                string taskType = theSimpleActionTask != null ? " (Simple Action)" : "";
                                string statusMessage =
                                    $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} Started Automatically{taskType}: {this.SelectedIndividualTask.TaskInfo.Description} ....";

                                int progressPercent = (i*100)/AllTasks.Count;
                                ((IProgress<Tuple<int, string>>) _progressObj).Report(
                                    new Tuple<int, string>(progressPercent, statusMessage));
                                addToLog(ActivityLog.LogTypes.Event, statusMessage);

                                await Task.Run(() =>
                                {
                                    this.SelectedIndividualTask.TaskInfo.ReturnedResponse = theSimpleActionTask != null 
                                        ? theSimpleActionTask.TaskAction.Invoke(_cancallationTokenSrc.Token) 
                                        : this.SelectedIndividualTask.CodedTask.Execute(_cancallationTokenSrc.Token);
                                });

                                addToLog(ActivityLog.LogTypes.TaskExecutionResult,
                                    this.SelectedIndividualTask.TaskInfo.ReturnedResponse);

                                this.SelectedIndividualTask = AllTasks[i];

                                this.SelectedIndividualTask.TaskInfo.EndTime = DateTime.Now;

                                this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.RanToCompletion;

                                progressPercent = ((i + 1)*100)/AllTasks.Count;
                                statusMessage = $"Task # {this.SelectedIndividualTask.TaskInfo.Sequence} Completed.";
                                ((IProgress<Tuple<int, string>>) _progressObj).Report(
                                    new Tuple<int, string>(progressPercent, statusMessage));
                                addToLog(ActivityLog.LogTypes.Event, statusMessage);
                                allTaskResults.Add(this.SelectedIndividualTask.TaskInfo);
                            }
                            catch (OperationCanceledException)
                            {
                                this.SelectedIndividualTask = AllTasks[i];

                                this.RunAllSequentialButtonText = "Resume Sequential Run";
                                this.RunAllSequentialButtonTooltip =
                                    "Resume executing rest of the tasks that did not run.";

                                this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Canceled;

                                string cancellationInfo =
                                    $"Cancelled task # {this.SelectedIndividualTask.TaskInfo.Sequence} : {this.SelectedIndividualTask.TaskInfo.Description}";

                                this.StatusBarMessage = cancellationInfo;
                                addToLog(ActivityLog.LogTypes.Warning, this.StatusBarMessage);

                                break;
                            }
                            catch (Exception err)
                            {
                                this.SelectedIndividualTask = AllTasks[i];

                                this.RunAllSequentialButtonText = "Resume Sequential Run";
                                this.RunAllSequentialButtonTooltip =
                                    "Resume executing rest of the tasks that did not run.";

                                this.SelectedIndividualTask.TaskInfo.Status = TaskRunStates.Faulted;
                                this.SelectedIndividualTask.TaskInfo.ThrownException = err;

                                exceptionThrown = true;
                                break;
                            }
                        }

                        return allTaskResults;
                    });

                    this.OverallProjectStatus = getOverallProjectStatus();

                    if (exceptionThrown)
                    {
                        string exceptionTitle =
                            string.Format(
                                SelectedIndividualTask.TaskInfo.ThrownException.GetType() +
                                " occured in Task # {0} : {1}",
                                this.SelectedIndividualTask.TaskInfo.Sequence,
                                this.SelectedIndividualTask.TaskInfo.Description);
                        addToLog(ActivityLog.LogTypes.Exception,
                            string.Format("{0}{1}{2}{1}{1}Stack Trace : {1}---------------------{1}{3}", exceptionTitle,
                                Environment.NewLine, SelectedIndividualTask.TaskInfo.ThrownException.Message,
                                SelectedIndividualTask.TaskInfo.ThrownException.StackTrace));
                        ShowExceptionDialog(SelectedIndividualTask.TaskInfo.ThrownException, exceptionTitle);
                    }

                    switch (this.OverallProjectStatus)
                    {
                        case TaskRunStates.RanToCompletion:
                            this.RunAllSequentialButtonText = "Run All Tasks";
                            this.RunAllSequentialButtonTooltip = "Run all tasks sequentially";
                            break;
                        case TaskRunStates.Faulted:
                            this.RunAllSequentialButtonText = "Resume Sequential Run";
                            this.RunAllSequentialButtonTooltip = "Resume executing rest of the tasks that did not run.";
                            break;
                        case TaskRunStates.Canceled:
                            this.RunAllSequentialButtonText = "Resume Sequential Run";
                            this.RunAllSequentialButtonTooltip = "Resume executing rest of the tasks that did not run.";
                            break;
                        default:
                            if (this.OverallProjectStatus == TaskRunStates.DidNotStart ||
                                AllTasks.All(x => x.TaskInfo.Status != TaskRunStates.DidNotStart))
                            {
                                this.RunAllSequentialButtonText = "Run All Tasks";
                                this.RunAllSequentialButtonTooltip = "Run all tasks sequentially";
                            }
                            break;
                    }

                    this.IsProjectDirty = true;
                    this.IsCancelationPending = false;

                    ProjectReport theProjectReport = new ProjectReport
                    {
                        TaskReports = theResults,
                        ProjectDescription = this.ProjectDescription,
                        TaskContainerDLLPath = this.ReceivedDLLFilePath,
                        SessionStartDateTime = theResults.Min(x => x.StartTime),
                        SessionEndDateTime = theResults.Max(x => x.EndTime),
                    };

                    try
                    {
                        this.LoadedTaskIndexFromAssembly.OnAllTasksCommandExecuted(theProjectReport);
                        addToLog(ActivityLog.LogTypes.Event, "Executed OnAllTasksCommandExecuted() overridable action");
                    }
                    catch (Exception err)
                    {
                        addToLog(ActivityLog.LogTypes.Exception,
                            "Error occured in your OnAllTasksCommandExecuted() method.");
                        ShowExceptionDialog(err, "Error occured in your OnAllTasksCommandExecuted() method.");
                    }

                    if (!statusMessageSet)
                    {
                        this.StatusBarMessage =
                            $"Completed tasks. Elapsed time : {theProjectReport.TotalElapsedTimeInSession}.";
                        addToLog(ActivityLog.LogTypes.Event, this.StatusBarMessage);
                    }
                }));
            }
        }

        /// <summary>
        /// Gets the show log window command.
        /// </summary>
        public RelayCommand ShowLogWindowCommand
        {
            get
            {
                return _showLogWindowCommand ?? (_showLogWindowCommand = new RelayCommand(() =>
                    Messenger.Default.Send(new ShowDialogWindowMessage(DialogFactory.DialogNames.LogViewer, this))));
            }
        }

        /// <summary>
        /// Gets the clear logs command.
        /// </summary>
        public RelayCommand ClearLogsCommand
        {
            get
            {
                return _clearLogsCommand ??
                       (_clearLogsCommand = new RelayCommand(() => Messenger.Default.Send(new DialogMessage(this,
                           "Are you sure that you want to clear all logs ?",
                           x =>
                           {
                               if (x == MessageBoxResult.Yes)
                               {
                                   this.SearchLogResetCommand.Execute(null);
                                   this._logs.Clear();

                                   OnPropertyChanged(nameof(TotalLogs));
                               }
                           }) {Button = MessageBoxButton.YesNo}), () => this.Logs.Any()));
            }
        }

        /// <summary>
        /// Gets the save log command.
        /// </summary>
        public RelayCommand SaveLogCommand
        {
            get
            {
                return _saveLogCommand ??
                       (_saveLogCommand = new RelayCommand(() => Messenger.Default.Send(new SaveFileMessage
                       {
                           Dialog = new SaveFileDialog
                           {
                               Filter = "Xml Files (*.xml)|*.xml|All Files (*.*)|*.*",
                               FilterIndex = 0,
                               OverwritePrompt = true,
                               Title = "Select a location to save the xml report.",
                               DefaultExt = ".xml",
                               AddExtension = true
                           },
                           CallBackWithSelectedFilePath = selectedFilePath =>
                           {
                               if (selectedFilePath != null)
                               {
                                   try
                                   {
                                       this.Logs.Serialize(selectedFilePath);
                                       addToLog(ActivityLog.LogTypes.Event,
                                           "Successfully saved the logs at " + selectedFilePath);

                                       Messenger.Default.Send(new ShowMessageBoxMessage("Successfully saved the logs!")
                                       {
                                           Title = "Success!"
                                       });
                                   }
                                   catch (Exception err)
                                   {
                                       addToLog(ActivityLog.LogTypes.Exception,
                                           $"Error saving the logs! {err.Message}");

                                       Messenger.Default.Send(new ShowMessageBoxMessage(err.Message)
                                       {
                                           Title = "Error!"
                                       });
                                   }
                               }
                           }
                       })));
            }
        }

        /// <summary>
        /// Gets the overall project status.
        /// </summary>
        /// <returns></returns>
        private TaskRunStates getOverallProjectStatus()
        {
            if (this.AllTasks.All(x => x.TaskInfo.Status == TaskRunStates.DidNotStart))
                return TaskRunStates.DidNotStart;
            if (this.AllTasks.Any(x => x.TaskInfo.Status == TaskRunStates.Faulted))
                return TaskRunStates.Faulted;
            if (this.AllTasks.All(x => x.TaskInfo.Status == TaskRunStates.RanToCompletion))
                return TaskRunStates.RanToCompletion;
            return TaskRunStates.Canceled;
        }

        /// <summary>
        /// Gets the cancel selected task command.
        /// </summary>
        public RelayCommand<int> CancelSelectedTaskCommand
        {
            get
            {
                return _cancelSelectedTaskCommand ?? (_cancelSelectedTaskCommand
                    = new RelayCommand<int>(taskSequence =>
                    {
                        this.SelectedIndividualTask =
                            this.AllTasks.SingleOrDefault(x => x.TaskInfo.Sequence == taskSequence);
                        if (this.SelectedIndividualTask == null)
                            return;

                        this.IsCancelationPending = true;
                        this.StatusBarMessage = SelectedTaskCancellationMessage.Replace("\n", "");
                        addToLog(ActivityLog.LogTypes.Event, this.StatusBarMessage);

                        _cancallationTokenSrc?.Cancel();

                        try
                        {
                            this.SelectedIndividualTask.CodedTask.Cancel();
                        }
                        catch (Exception err)
                        {
                            ShowExceptionDialog(err, "Error occured in your CodedTask's Cancel() method.");
                        }
                    }, x => !this.IsCancelationPending));
            }
        }

        /// <summary>
        /// Gets the cancel automatic task run command.
        /// </summary>
        public RelayCommand CancelAllTaskRunCommand
        {
            get
            {
                return _cancelAllTaskRunCommand ?? (_cancelAllTaskRunCommand
                    = new RelayCommand(() =>
                    {
                        this.StatusBarMessage = BatchTaskCancellationMessage.Replace("\n", "");
                        addToLog(ActivityLog.LogTypes.Event, this.StatusBarMessage);

                        this.IsCancelationPending = true;
                        _cancallationTokenSrc?.Cancel();

                        try
                        {
                            this.SelectedIndividualTask.CodedTask.Cancel();
                        }
                        catch (Exception err)
                        {
                            ShowExceptionDialog(err, "Error occured in your CodedTask's Cancel() method.");
                        }
                    }, () => !this.IsCancelationPending));
            }
        }

        /// <summary>
        /// Gets the reset all task status command.
        /// </summary>
        public RelayCommand ResetAllTaskStatusCommand
        {
            get
            {
                return _resetAllTaskStatusCommand ?? (_resetAllTaskStatusCommand = new RelayCommand(() =>
                {
                    if (this.AllTasks != null && this.AllTasks.Count > 0)
                    {
                        foreach (DecoratedCodedTask aTask in this.AllTasks)
                        {
                            aTask.TaskInfo.Status = TaskRunStates.DidNotStart;
                            aTask.TaskInfo.ReturnedResponse = null;
                            aTask.TaskInfo.StartTime = null;
                            aTask.TaskInfo.EndTime = null;
                            aTask.TaskInfo.ThrownException = null;
                        }

                        this.IsProjectDirty = false;
                        this.OverallProjectStatus = TaskRunStates.DidNotStart;
                        this.RunAllSequentialButtonText = "Run All Tasks";
                        this.RunAllSequentialButtonTooltip = "Run all tasks sequentially";

                        addToLog(ActivityLog.LogTypes.Event, "All Task Status has been RESET");
                    }
                }));
            }
        }

        /// <summary>
        /// Gets the change plugin DLL path command.
        /// </summary>
        public RelayCommand ChangePluginDLLPathCommand
        {
            get
            {
                return _changePluginDllPathCommand ?? (_changePluginDllPathCommand
                    = new RelayCommand(() => Messenger.Default.Send(new OpenFileMessage
                    {
                        Dialog = new OpenFileDialog
                        {
                            Filter = "DLL Files (*.dll)|*.dll",
                            FilterIndex = 0,
                            Title = "Select the Plugin container DLL file.",
                        },
                        CallBackWithSelectedFilePath = selectedFilePath =>
                        {
                            if (selectedFilePath != null)
                                this.ReceivedDLLFilePath = selectedFilePath;
                        }
                    })));
            }
        }

        /// <summary>
        /// Gets the configure vs project for debug command.
        /// </summary>
        public RelayCommand ConfigureVSProjectForDebugCommand
        {
            get
            {
                return _configureVsProjectForDebugCommand ?? (_configureVsProjectForDebugCommand
                    = new RelayCommand(() =>
                    {
                        string fileDialogInitialDir = null;
                        try
                        {
                            string theProjectdirectory =
                                Path.GetDirectoryName(
                                    Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)));
                            if (theProjectdirectory != null && Directory.GetFiles(theProjectdirectory).Any(x =>
                            {
                                string extension = Path.GetExtension(x);
                                return !string.IsNullOrEmpty(extension) &&
                                       extension.EndsWith("proj", StringComparison.OrdinalIgnoreCase);
                            }))
                            {
                                fileDialogInitialDir = theProjectdirectory;
                            }
                        }
                        catch
                        {
                            // ignored
                        }

                        OpenFileDialog theDialog = new OpenFileDialog
                        {
                            Title = "Select the Visual Studio Project file.",
                            Filter =
                                "C# Projects (*.csproj)|*.csproj|VB Projects(*.vbproj)|*.vbproj|All Files (*.*)|*.*"
                        };
                        if (fileDialogInitialDir != null)
                            theDialog.InitialDirectory = fileDialogInitialDir;

                        Messenger.Default.Send(new OpenFileMessage
                        {
                            Dialog = theDialog,
                            CallBackWithSelectedFilePath = selectedFilePath =>
                            {
                                if (selectedFilePath != null)
                                {
                                    try
                                    {
                                        string statusMessage = "";

                                        string theUserFile = selectedFilePath + ".user";
                                        if (File.Exists(theUserFile))
                                        {
                                            using (
                                                MsBuildScriptService serviceForDeleteFromUserfile =
                                                    new MsBuildScriptService(theUserFile))
                                            {
                                                serviceForDeleteFromUserfile
                                                    .RemoveStartProgramInfoFromMsBuildVsProjectUserFile();
                                            }

                                            statusMessage += "The Startup Info from User file has been deleted. ";
                                        }

                                        using (
                                            MsBuildScriptService myService = new MsBuildScriptService(selectedFilePath))
                                        {
                                            myService.UpdateStartProgramInfoInMsBuildVsProjectFile();
                                        }

                                        statusMessage += "The Startup Info in the main project file has been updated.";

                                        this.StatusBarMessage = statusMessage;
                                        addToLog(ActivityLog.LogTypes.Event, statusMessage);
                                    }
                                    catch (Exception err)
                                    {
                                        ShowExceptionDialog(err, "Error updating your Visual Studio Project file.");
                                    }
                                }
                            }
                        });
                    }));
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this._progressObj.ProgressChanged -= progressObjProgressObjChanged;
            if (this.AllTasks != null)
            {
                foreach (DecoratedCodedTask aTask in this.AllTasks)
                {
                    aTask.CodedTask.ProgressChanged -= CodedTask_ProgressChanged;
                    aTask.CodedTask.StatusMessageUpdated -= CodedTask_StatusMessageUpdated;
                    aTask.CodedTask.Dispose();
                }
            }
        }
    }
}