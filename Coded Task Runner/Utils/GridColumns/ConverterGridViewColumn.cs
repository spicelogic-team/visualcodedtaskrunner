using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace SpiceLogic.CodedTaskRunner.Utils.GridColumns
{
	public abstract class ConverterGridViewColumn : GridViewColumn, IValueConverter
	{
		protected ConverterGridViewColumn( Type bindingType )
		{
		    if (bindingType == null)
		        throw new ArgumentNullException("bindingType");

		    this.bindingType = bindingType;

			Binding binding = new Binding {Mode = BindingMode.OneWay, Converter = this};
		    DisplayMemberBinding = binding;
		}

		public Type BindingType
		{
			get { return bindingType; }
		} 

		protected abstract object ConvertValue( object value );

		object IValueConverter.Convert( object value, Type targetType, object parameter, CultureInfo culture )
		{
		    if (!bindingType.IsInstanceOfType(value))
		        throw new InvalidOperationException();
		    return ConvertValue( value );
		} 

		object IValueConverter.ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
		{
			throw new NotImplementedException();
		} 

		private readonly Type bindingType;
	} 
} 