﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SpiceLogic.CodedTaskRunner.Models;
using SpiceLogic.CodedTaskRunner.MVVMCore.Messaging;
using SpiceLogic.CodedTaskRunner.Utils;
using SpiceLogic.CodedTaskRunner.ViewModels;
using SpiceLogic.CodedTaskRunner.Views.Dialogs;

namespace SpiceLogic.CodedTaskRunner.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    internal partial class MainWindow
    {
        private MainViewModel _theViewModel;
        private readonly SynchronizationContext _mSynchronizationContext;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            _mSynchronizationContext = SynchronizationContext.Current;

            Messenger.Default.Register<ShowDialogWindowMessage>(this, showDialogTask);
            Messenger.Default.Register<ShowMessageBoxMessage>(this, showMesageBoxTask);
            Messenger.Default.Register<NotificationMessage>(this, MessageNames.RestartApplicationForNewDLL, x => restartApplication(x.Notification));
            Messenger.Default.Register<NotificationMessage>(this, MessageNames.FocusAndSelectFirstTask, x => focusAndSelectFirstTask());
            Messenger.Default.Register<SaveFileMessage>(this, showSaveFileDialogTask);
            Messenger.Default.Register<OpenFileMessage>(this, showOpenFileDialogTask);
            Messenger.Default.Register<NotificationMessage<DecoratedCodedTask>>(this, MessageNames.ScrollListToView, x => scrollListToView(x.Content));

            loadUiPreferenceFromSettings();
        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _theViewModel = (MainViewModel)this.DataContext;
            _theViewModel.ReceivedDLLFilePath = App.ReceivedExtensionDllPath;
            if (_theViewModel.LoadedTaskIndexFromAssembly != null)
            {
                try
                {
                    _theViewModel.LoadedTaskIndexFromAssembly.OnApplicationLoaded();
                }
                catch (Exception err)
                {
                    _theViewModel.ShowExceptionDialog(err, "Error occured in your OnApplicationLoaded() method.");
                }
            }
        }

        /// <summary>
        /// Scrolls the list automatic view.
        /// </summary>
        /// <param name="task">The task.</param>
        private void scrollListToView(DecoratedCodedTask task)
        {
            _mSynchronizationContext.Post(x => lstViewTaskGrid.ScrollIntoView(task), null); 
        }

        /// <summary>
        /// Focuses the and select first task.
        /// </summary>
        private void focusAndSelectFirstTask()
        {
            lstViewTaskGrid.Focus();
            if (lstViewTaskGrid.Items.Count > 0)
                lstViewTaskGrid.SelectedIndex = 0;
        }

        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        private static void showDialogTask(ShowDialogWindowMessage msg)
        {
            DialogFactory.DialogWindowResult dlgResult = DialogFactory.ShowDialog(msg.DialogName, msg.WindowDataContext);
            if (dlgResult.OkCancelResult == DialogFactory.DialogOKCancelResult.Ok)
                msg.CallBack?.Invoke(dlgResult);
        }

        /// <summary>
        /// Shows the save file dialog task.
        /// </summary>
        /// <param name="obj">The object.</param>
        private static void showSaveFileDialogTask(SaveFileMessage obj)
        {
            obj.CallBackWithSelectedFilePath(obj.Dialog.ShowDialog() == true ? obj.Dialog.FileName : null);
        }

        /// <summary>
        /// Shows the open file dialog task.
        /// </summary>
        /// <param name="obj">The obj.</param>
        private static void showOpenFileDialogTask(OpenFileMessage obj)
        {
            obj.CallBackWithSelectedFilePath(obj.Dialog.ShowDialog() == true ? obj.Dialog.FileName : null);
        }

        /// <summary>
        /// Shows the mesage box task.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        private static void showMesageBoxTask(ShowMessageBoxMessage msg)
        {
            MessageBox.Show(msg.Message, msg.Title);
        }

        /// <summary>
        /// Restarts the application.
        /// </summary>
        /// <param name="dllPath">The DLL path.</param>
        private static void restartApplication(string dllPath)
        {
            string theExeLocation = Assembly.GetExecutingAssembly().Location;
            string theArg = dllPath;
            try
            {
                Process.Start(new ProcessStartInfo(theExeLocation, $"\"{theArg}\""));
                Application.Current.Shutdown();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error restarting the application.");
            }
        }

        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            _theViewModel = (MainViewModel)this.DataContext;

            Messenger.Default.Unregister<ShowMessageBoxMessage>(this, showMesageBoxTask);
            Messenger.Default.Unregister<NotificationMessage>(this, MessageNames.RestartApplicationForNewDLL, x => restartApplication(x.Notification));
            Messenger.Default.Unregister<NotificationMessage>(this, MessageNames.FocusAndSelectFirstTask, x => focusAndSelectFirstTask());
            Messenger.Default.Unregister<SaveFileMessage>(this, showSaveFileDialogTask);
            Messenger.Default.Unregister<NotificationMessage<DecoratedCodedTask>>(this, MessageNames.ScrollListToView, x => scrollListToView(x.Content));

            if (_theViewModel.LoadedTaskIndexFromAssembly != null)
            {
                try
                {
                    _theViewModel.LoadedTaskIndexFromAssembly.OnApplicationClosing();
                }
                catch (Exception err)
                {
                    _theViewModel.ShowExceptionDialog(err, "Error occured in your OnApplicationClosing() method.");
                }
            }

            _theViewModel.Dispose();

            saveUiPreferenceInSettings();
        }

        /// <summary>
        /// Saves the unique identifier settings.
        /// </summary>
        private void saveUiPreferenceInSettings()
        {
            try
            {
                if (lstViewTaskGrid.View is GridView theGrid)
                {
                    Dictionary<string, double> theColumnInfoList = theGrid.Columns.ToDictionary(aColumn => aColumn.Header.ToString(), aColumn => aColumn.Width);
                    string theSerializedValue = theColumnInfoList.Serialize();
                    Properties.Settings.Default.TaskGridColumnOrderSerialized = theSerializedValue;
                }

                Properties.Settings.Default.WindowHeight = this.ActualHeight;
                Properties.Settings.Default.WindowWidth = this.ActualWidth;
                Properties.Settings.Default.Save();
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Loads the column orders from settings.
        /// </summary>
        private void loadUiPreferenceFromSettings()
        {
            try
            {
                Dictionary<string, double> theColumnInfoList = Serialization.DeserializeFromXmlStr<Dictionary<string, double>>(Properties.Settings.Default.TaskGridColumnOrderSerialized);

                if (theColumnInfoList == null || theColumnInfoList.Count <= 0)
                    return;
                if (!(lstViewTaskGrid.View is GridView theGrid))
                    return;
                int newIndex = 0;
                foreach (string colName in theColumnInfoList.Keys)
                {
                    int oldIndex = 0;
                    for (int i = 0; i < theGrid.Columns.Count; i++)
                    {
                        if (!theGrid.Columns[i].Header.ToString().Equals(colName))
                            continue;
                        oldIndex = i;
                        if (!string.IsNullOrWhiteSpace(colName) && theColumnInfoList.TryGetValue(colName, out double colwidth))
                            theGrid.Columns[i].Width = colwidth;
                        break;
                    }

                    theGrid.Columns.Move(oldIndex, newIndex++);
                }

                if (Properties.Settings.Default.WindowHeight > 0)
                    this.Height = Properties.Settings.Default.WindowHeight;
                if (Properties.Settings.Default.WindowWidth > 0)
                    this.Width = Properties.Settings.Default.WindowWidth;
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Handles the Click event of the menuExit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Handles the Click event of the menuCheckForUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuCheckForUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(UrlFactory.CheckForUpdateUrl.AbsoluteUri);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error opening external website");
            }
        }

        /// <summary>
        /// Handles the Click event of the menuDocumentation control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuDocumentation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(UrlFactory.DocumentationUrl.AbsoluteUri);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error opening external website");
            }
        }

        /// <summary>
        /// Handles the Click event of the menuContactForSupport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuContactForSupport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(UrlFactory.HelpDeskUrl.AbsoluteUri);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error opening external website");
            }
        }

        /// <summary>
        /// Handles the Click event of the menuAbout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuAbout_Click(object sender, RoutedEventArgs e)
        {
            DialogFactory.ShowDialog(DialogFactory.DialogNames.About, new AboutViewModel());
        }

        /// <summary>
        /// Handles the Click event of the menuCreateMetadataFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menuCreateMetadataFile_Click(object sender, RoutedEventArgs e)
        {
            MetadataFileEditorForStartupDLL theDialog = new MetadataFileEditorForStartupDLL(this._theViewModel.ReceivedDLLFilePath != null ? Path.GetFileName(this._theViewModel.ReceivedDLLFilePath) : "");
            if (theDialog.ShowDialog() == true)
            {
                string executingAssemblyFolder = AppDomain.CurrentDomain.BaseDirectory;
                string writingFilePath = Path.Combine(executingAssemblyFolder, AppConfigs.StartUpDLLFileNameContainerMetadataFileName);
                try
                {
                    File.WriteAllText(writingFilePath, theDialog.DLLFileName);
                    _theViewModel.StatusBarMessage = "The metadata file has been created at " + writingFilePath;
                }
                catch (Exception)
                {
                    MessageBox.Show(string.Format("Oops! Looks like this App does not have permission to write file at {0}{1}.{0} Please create a text file at: {0}{1}{0} and simply place the following text in that file : {2}{0}{0}Once you do that, it will be enough for this application to understand which dll file to load automatically.", Environment.NewLine, writingFilePath, theDialog.DLLFileName));
                }
            }
        }

        #region --- Copy from List View ---

/*
        /// <summary>
        /// CTRLs the C copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ListView lb = (ListView)(sender);
            DecoratedCodedTask selected = (DecoratedCodedTask) lb.SelectedItem;
            if (selected != null)
                Clipboard.SetText(selected.TaskInfo.Description);
        }
*/

/*
        /// <summary>
        /// CTRLs the C copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void CtrlCCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
*/

        /// <summary>
        /// Rights the click copy CMD executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            DecoratedCodedTask selected = (DecoratedCodedTask)mi.DataContext;
            if (selected != null)
                Clipboard.SetText(selected.TaskInfo.Description);
        }

        /// <summary>
        /// Rights the click copy CMD can execute.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void RightClickCopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        #endregion

        /// <summary>
        /// Handles the KeyDown event of the txtSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                _theViewModel.SearchCommand.Execute(e);
        }
    }
}
