﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace SpiceLogic.CodedTaskRunner.Utils
{
    internal class MsBuildScriptService : IDisposable
    {
        private readonly string _msBuildProjectFilePath;
        readonly XNamespace _nameSpace = "";
        private XDocument _doc;

        /// <summary>
        /// Initializes a new instance of the <see cref="MsBuildScriptService"/> class.
        /// </summary>
        /// <param name="msBuildProjectFilePath">The parameters build project file path.</param>
        public MsBuildScriptService(string msBuildProjectFilePath)
        {
            _msBuildProjectFilePath = msBuildProjectFilePath;
            _doc = XDocument.Load(_msBuildProjectFilePath);
            XAttribute thexmlNsAttribute = _doc.Root?.Attribute("xmlns");
            if (thexmlNsAttribute != null)
                _nameSpace = thexmlNsAttribute.Value;
        }

        /// <summary>
        /// Reads the project file.
        /// </summary>
        /// <returns></returns>
        public void UpdateStartProgramInfoInMsBuildVsProjectFile()
        {
            if (_doc.Root != null)
            {
                XElement[] conditionPropertyElements = _doc.Root
                    .Elements()
                    .Where(anElement => anElement.Name.LocalName.Equals("PropertyGroup", StringComparison.OrdinalIgnoreCase)
                                        && anElement.Attribute("Condition") != null
                                        && anElement.Attribute("Condition").Value.ToLower().Contains("$(configuration)|$(platform)"))
                    .ToArray();

                processASingleItemUnderConditionGroup(conditionPropertyElements, "StartAction", "Program");
                processASingleItemUnderConditionGroup(conditionPropertyElements, "StartProgram", "$(MSBuildProjectDirectory)\\$(OutputPath)SpiceLogicCodedTaskRunner.exe");
                processASingleItemUnderConditionGroup(conditionPropertyElements, "StartArguments", "\"$(MSBuildProjectDirectory)\\$(OutputPath)$(AssemblyName)\"");
                _doc.Save(_msBuildProjectFilePath);
            }
        }

        /// <summary>
        /// Removes the start program information from parameters build vs project user file.
        /// </summary>
        public void RemoveStartProgramInfoFromMsBuildVsProjectUserFile()
        {
            if (_doc.Root != null)
            {
                XElement[] conditionPropertyElements = _doc.Root
                    .Elements()
                    .Where(anElement => anElement.Name.LocalName.Equals("PropertyGroup", StringComparison.OrdinalIgnoreCase)
                                        && anElement.Attribute("Condition") != null
                                        && anElement.Attribute("Condition").Value.ToLower().Contains("$(configuration)|$(platform)"))
                    .ToArray();

                deleteASingleItemUnderConditionGroup(conditionPropertyElements, "StartAction");
                deleteASingleItemUnderConditionGroup(conditionPropertyElements, "StartProgram");
                deleteASingleItemUnderConditionGroup(conditionPropertyElements, "StartArguments");
                _doc.Save(_msBuildProjectFilePath);
            }
        }

        /// <summary>
        /// Processes the aggregate single item under condition group.
        /// </summary>
        /// <param name="conditionPropertyElements">The condition property elements.</param>
        /// <param name="elementName">Name of the element.</param>
        private static void deleteASingleItemUnderConditionGroup(IEnumerable<XElement> conditionPropertyElements, string elementName)
        {
            foreach (XElement anElement in conditionPropertyElements)
            {
                XElement theElement = anElement.Elements().FirstOrDefault(x => x.Name.LocalName.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                theElement?.Remove();
            }
        }

        /// <summary>
        /// Processes the aggregate single item under condition group.
        /// </summary>
        /// <param name="conditionPropertyElements">The condition property elements.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="elementValue">The element value.</param>
        private void processASingleItemUnderConditionGroup(IEnumerable<XElement> conditionPropertyElements,
            string elementName, string elementValue)
        {
            foreach (XElement anElement in conditionPropertyElements)
            {
                XElement theelement = anElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                if (theelement != null)
                    theelement.Value = elementValue;
                else
                    anElement.Add(new XElement(_nameSpace + elementName) { Value = elementValue });
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _doc = null;
        }
    }
}