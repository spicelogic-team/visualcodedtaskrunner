﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Threading;
using SpiceLogic.CodedTaskRunner.ViewModels;
using SpiceLogic.CodedTaskRunner.Views;

namespace SpiceLogic.CodedTaskRunner
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    internal partial class App
    {
        public App()
        {
            Current.DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        public static string ReceivedExtensionDllPath;

        /// <summary>
        /// Handles the Startup event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.StartupEventArgs"/> instance containing the event data.</param>
        private void Application_Startup(object sender, System.Windows.StartupEventArgs e)
        {
            if (e.Args.Any())
            {
                string argName = e.Args[0];
                if (!argName.EndsWith(".dll", StringComparison.OrdinalIgnoreCase))
                    argName += ".dll";
                ReceivedExtensionDllPath = argName;
            }
            else
            {
                string metadataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppConfigs.StartUpDLLFileNameContainerMetadataFileName);

                if (File.Exists(metadataFilePath))
                {
                    string startupDllFileName = File.ReadAllText(metadataFilePath);

                    string dllFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, startupDllFileName);

                    if (!dllFilePath.EndsWith(".dll", StringComparison.OrdinalIgnoreCase))
                        dllFilePath += ".dll";

                    if (File.Exists(dllFilePath))
                    {
                        ReceivedExtensionDllPath = dllFilePath;
                        //string metadataContent = File.ReadAllText(metadataFilePath).Trim();
                        //if (File.Exists(metadataContent))
                        //    ReceivedExtensionDllPath = metadataContent;
                        //else
                        //{
                        //    string loadingDLLFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, metadataContent);
                        //    if (File.Exists(loadingDLLFilePath))
                        //        ReceivedExtensionDllPath = loadingDLLFilePath;
                        //}
                    }
                }
            }
        }

        static void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG   // In debug mode do not custom-handle the exception, let Visual Studio handle it
            e.Handled = false;
#else
            ShowUnhandeledException(e);    
#endif
        }

        private static void ShowUnhandeledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            ExceptionViewModel errViewModel = new ExceptionViewModel(e.Exception, "Error occured in Application Level");
            DialogFactory.ShowDialog(DialogFactory.DialogNames.TaskException, errViewModel);
        }
    }
}
