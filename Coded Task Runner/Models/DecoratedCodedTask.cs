﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using SpiceLogic.CodedTaskRunner.Annotations;

namespace SpiceLogic.CodedTaskRunner.Models
{
    internal class DecoratedCodedTask : INotifyPropertyChanged
    {
        private CodedTaskReport _taskInfo;

        /// <summary>
        /// Gets or sets the task.
        /// </summary>
        public CodedTaskBase CodedTask { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public CodedTaskReport TaskInfo
        {
            get => _taskInfo;
            set
            {
                if (!Equals(_taskInfo, value))
                {
                    _taskInfo = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}