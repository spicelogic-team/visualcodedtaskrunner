﻿namespace SpiceLogic.CodedTaskRunner.Views.UserControls.TaskStatusVisualizers
{
    /// <summary>
    /// Interaction logic for CanceledVisualizer.xaml
    /// </summary>
    public partial class CanceledVisualizer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CanceledVisualizer"/> class.
        /// </summary>
        public CanceledVisualizer()
        {
            InitializeComponent();
        }
    }
}
