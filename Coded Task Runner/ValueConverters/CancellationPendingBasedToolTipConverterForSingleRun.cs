﻿using System;
using System.Globalization;
using System.Windows.Data;
using SpiceLogic.CodedTaskRunner.ViewModels;

namespace SpiceLogic.CodedTaskRunner.ValueConverters
{
    internal class CancellationPendingBasedToolTipConverterForSingleRun : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isCancellationPending = value != null && (bool)value;
            return isCancellationPending ? MainViewModel.SelectedTaskCancellationMessage : "Cancel";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}