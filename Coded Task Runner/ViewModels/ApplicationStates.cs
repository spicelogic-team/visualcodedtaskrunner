﻿namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationStates
    {
        /// <summary>
        /// Gets or sets a value indicating whether [already loaded plugin from assembly].
        /// </summary>
        /// <value>
        /// <c>true</c> if [already loaded plugin from assembly]; otherwise, <c>false</c>.
        /// </value>
        public static bool AlreadyLoadedPluginFromAssembly { get; set; }
    }
}