﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using SpiceLogic.CodedTaskRunner;
using SpiceLogic.CodedTaskRunner.ClientAttributes;
using SpiceLogic.CodedTaskRunner.Models;

namespace SampleTaskPlugin2
{
    [Description("Continuous Integration Tasks for Product YYYYYYYYYYYY")]
    public class TaskIndex : CodedTaskIndexBase
    {
        public override IList<CodedTaskBase> ListAllCodedTasks()
        {
            List<CodedTaskBase> myTasks = new List<CodedTaskBase>
            {
                new CopyFilesTask(), 
                new CopyFolderTask(),
                new CreateZipFileTask(),
                new SendEmailTask()
            };

            return myTasks;
        }

        public class MyInputType : UserInputItem
        {
            public int Age { get; set; }

            public string Name { get; set; }

            public override string DisplayText => Name;
        }

        public override UserInputSetting GetInputSelectionItems()
        {
            IList<UserInputItem> list = new List<UserInputItem>
            {
                new MyInputType {Name = "James", Age = 21},
                new MyInputType {Name = "Rock", Age = 11},
                new MyInputType {Name = "Nishat", Age = 43}
            };

            return new UserInputSetting {SelectionInputLabel = "Select Product", SelectionItems = list, SelectedItemDisplayText = "rock"};
        }

        public override void OnInputSelectionChanged(UserInputItem selectedItem)
        {
            MyInputType mySelectedItem = (MyInputType) selectedItem;
            MessageBox.Show("Selected Item : " + mySelectedItem.DisplayText + ", age -> " + mySelectedItem.Age);
        }
    }

    [Description("Copy Files")]
    public class CopyFilesTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            int totalCopied = 0;
            
            for (int i = 0; i < 20; i++)
            {
                cancellationToken.ThrowIfCancellationRequested();

                Thread.Sleep(100);
                totalCopied++;
            }

            return $"Successfully copied {totalCopied} files";
        }
    }

    [Description("Copy Folder")]
    public class CopyFolderTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            int totalCopied = 0;
            
            for (int i = 0; i < 20; i++)
            {
                cancellationToken.ThrowIfCancellationRequested();

                Thread.Sleep(100);
                totalCopied++;
            }

            return $"Successfully copied {totalCopied} folders";
        }
    }

    [AttendedTaskAttribute]
    [Description("This is TaskBase C (Sample 2)")]
    public class CreateZipFileTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
           
            Thread.Sleep(1000);
          
            return "Successfully made the zip file with 12 files";
        }
    }

    [Description("This is TaskBase D (Sample 2)")]
    public class TaskBaseD : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            const int x = 4;
            if (x < 6)
                throw new Exception(
                    "This is an intentional exception in order to simulate a situation when the TaskBase Fails.");
        }
    }

    [Description("Send Email to the Project Manager about Product Release")]
    public class SendEmailTask: CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            Thread.Sleep(1000);

            return "Successfully sent email to project.manager@hotmail.com";
        }
    }
}
