﻿using System;
using System.Web;
using SpiceLogic.CodedTaskRunner.Utils;

namespace SpiceLogic.CodedTaskRunner
{
    internal static class UrlFactory
    {
        public static readonly Uri MainWebsiteUrl = new Uri("http://www.spicelogic.com");
        public static readonly Uri DocumentationUrl = new Uri("http://www.spicelogic.com/docs/" + AssemblyInfo.ProductKey);
        public static readonly Uri HelpDeskUrl = new Uri("http://www.spicelogic.com/HelpDesk");
        public static readonly Uri CheckForUpdateUrl = new Uri(
            $"http://www.spicelogic.com/Products/Check4Update?productKey={AssemblyInfo.ProductKey}&version={HttpUtility.UrlEncode(AssemblyInfo.Version)}");
    }
}
