﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using SpiceLogic.CodedTaskRunner.Annotations;
using SpiceLogic.CodedTaskRunner.Utils;

namespace SpiceLogic.CodedTaskRunner.Models
{
    /// <summary>
    /// Coded Task Report.
    /// </summary>
    [DataContract]
    public class CodedTaskReport : INotifyPropertyChanged
    {
        private int _sequence;
        private string _name;
        private string _fullName;
        private string _description;
        private bool _isAttendedTask;
        private string _returnedResponse;
        private DateTime? _startTime;
        private DateTime? _endTime;
        private TaskRunStates _status;
        private Exception _thrownException;
        private bool _isManualInstruction;
        private int _runningTaskProgress;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodedTaskReport"/> class.
        /// </summary>
        public CodedTaskReport()
        {
            this.Status = TaskRunStates.DidNotStart;
        }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        [DataMember(Order = 0)]
        public int Sequence
        {
            get => _sequence;
            set
            {
                if (_sequence != value)
                {
                    _sequence = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the task.
        /// </summary>
        [DataMember(Order = 1)]
        public string Name
        {
            get => _name;
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the fully qualified.
        /// </summary>
        [DataMember(Order = 2)]
        public string FullName
        {
            get => _fullName;
            set
            {
                if (_fullName != value)
                {
                    _fullName = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [DataMember(Order = 3)]
        public string Description
        {
            get => _description;
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the returned result.
        /// </summary>
        [DataMember(Order = 4)]
        public string ReturnedResponse
        {
            get => _returnedResponse;
            set
            {
                if (_returnedResponse != value)
                {
                    _returnedResponse = value;
                    OnPropertyChanged();
                    OnPropertyChanged("ReturnedResponseLines"); //todo: Suspecting name. Maybe it is not valid anymroe.
                }
            }
        }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        [DataMember(Order = 5)]
        public DateTime? StartTime
        {
            get => _startTime;
            set
            {
                if (_startTime != value)
                {
                    _startTime = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        [DataMember(Order = 6)]
        public DateTime? EndTime
        {
            get => _endTime;
            set
            {
                if (_endTime != value)
                {
                    _endTime = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the elapsed time information seconds.
        /// </summary>
        [DataMember(Order = 7)]
        public string ElapsedTime
        {
            get {
                return this.StartTime.HasValue && this.EndTime.HasValue
                    ? CommonUtils.GetElapsedTime(this.StartTime.Value, this.EndTime.Value)
                    : string.Empty;
            }
            // ReSharper disable once ValueParameterNotUsed
            internal set { }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [DataMember(Order = 8)]
        public TaskRunStates Status
        {
            get => _status;
            set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [should break running next automatic task].
        /// </summary>
        [DataMember(Order = 9)]
        public bool IsAttendedTask
        {
            get => _isAttendedTask;
            set
            {
                if (_isAttendedTask != value)
                {
                    _isAttendedTask = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [is manual instruction].
        /// </summary>
        [DataMember(Order = 10)]
        public bool IsManualInstruction
        {
            get => _isManualInstruction;
            set
            {
                if (_isManualInstruction != value)
                {
                    _isManualInstruction = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the thrown exception.
        /// </summary>
        [IgnoreDataMember]
        public Exception ThrownException
        {
            get => _thrownException;
            set
            {
                if (_thrownException != value)
                {
                    _thrownException = value;
                    this.ThrownExceptionMessage = this.ThrownException == null ? null : value.Message;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the running task progress. 
        /// Please note : This property name is hardcoded in the File Views\UserControls\TaskStatusVisualizers\RunningVisualizer.xaml.cs
        /// So, if you change this property name, make sure to sync in that file.
        /// </summary>
        [IgnoreDataMember]
        internal int RunningTaskProgress
        {
            get => _runningTaskProgress;
            set
            {
                if (_runningTaskProgress != value)
                {
                    _runningTaskProgress = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the thrown exception message.
        /// </summary>
        /// <value>
        /// The thrown exception message.
        /// </value>
        [DataMember(Order = 11)]
        public string ThrownExceptionMessage { get; set; }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}