﻿using System;

namespace SpiceLogic.CodedTaskRunner.ClientAttributes
{
    /// <summary>
    /// Use this Attribute to mark a Coded Task as Attended Task.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AttendedTaskAttribute : Attribute
    {
    }
}
