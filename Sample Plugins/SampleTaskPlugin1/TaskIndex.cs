﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using SpiceLogic.CodedTaskRunner;
using SpiceLogic.CodedTaskRunner.ClientAttributes;
using SpiceLogic.CodedTaskRunner.Models;

namespace SampleTaskPlugin1
{
    [Description("Continuous Integration Tasks for Product XXXXXXXX")]
    public class TaskIndex : CodedTaskIndexBase
    {
        public override IList<CodedTaskBase> ListAllCodedTasks()
        {
            List<CodedTaskBase> myTasks = new List<CodedTaskBase>
            {
                new CopyFilesTask(),
                new CopyFolderTask(),
                new CreateZipFileTask(),
                new SimpleActionTask("This is a simple action, simple open the Web Browser with Google",
                    x =>
                    {
                        Process.Start("http://www.google.ca");
                        return "Successfully opened Google";
                    }, true),
                new SimpleActionTask("math division task",
                    x =>
                    {
                        const int m = 5;
                        int y = 0;
                        int z = m/y;
                        return "divition done";
                    }),
                new ManualInstructionTask("Do this and then do that, ok ? please dont forget"),
                new TaskBaseD(),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000), isAttendedTask: true),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SimpleActionTask("Jekhane simanto", x => Thread.Sleep(1000)),
                new SendEmailTask(),
            };

            return myTasks;
        }

        ///// <summary>
        ///// Called when all tasks command executed.
        ///// </summary>
        ///// <param name="executionReport">The execution report.</param>
        //public override void OnAllTasksCommandExecuted(ProjectReport executionReport)
        //{
        //    MessageBox.Show(string.Format("Hello from the Plugin DLL. I have been notified that you completed {0} tasks. in {1} seconds",
        //        executionReport.TaskReports.Count, executionReport.TaskReports.Where(x => x.Status != TaskRunStates.DidNotStart)
        //        .Sum(x => (x.StartTime.HasValue && x.EndTime.HasValue) ? (x.EndTime.Value - x.StartTime.Value).TotalSeconds : 0)));
        //}

        ///// <summary>
        ///// Called when manually executed individual task.
        ///// </summary>
        ///// <param name="executionReport">The execution report.</param>
        //public override void OnManuallyExecutedIndividualTask(CodedTaskReport executionReport)
        //{
        //    MessageBox.Show(string.Format("Hello from the Plugin DLL. I have been notified that you completed task # {0} \"{1}\" in {2}",
        //        executionReport.Sequence, executionReport.Description, executionReport.ElapsedTime));
        //}

        /// <summary>
        /// Listses the extra toolbar buttons.
        /// </summary>
        /// <returns></returns>
        public override IList<UserToolbarButton> ListsExtraToolbarButtons()
        {
            List<UserToolbarButton> myButtons = new List<UserToolbarButton>
            {
                new UserToolbarButton
                {
                    ButtonText = "Open Publish Folder",
                    ImageDimension = 16,
                    Tooltip = "test publsh",
                    ToolbarImageSource = new Uri("/Resources/openFolder24.png", UriKind.Relative),
                    ClickAction = () =>
                    {
                        MessageBox.Show("Clicked Open Publish Folder (action)");
                        throw new Exception("What is life ?");
                    }
                },
                new UserToolbarButton
                {
                    ImageDimension = 16,
                    Tooltip = "Do All",
                    ToolbarImageSource = new Uri("/Resources/run_all16.png", UriKind.Relative)
                }
            };

            return myButtons;
        }
    }

    [Description("Copy Files (Sample 1)")]
    public class CopyFilesTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            int totalCopied = 0;
            Task.Delay(1000, cancellationToken);
            for (int i = 0; i < 20; i++)
            {
                cancellationToken.ThrowIfCancellationRequested();

                Thread.Sleep(100);
                totalCopied++;
            }

            return
                string.Format("Successfully copied {0} files" + Environment.NewLine + "Nothing was serious",
                    totalCopied);
        }
    }

    [Description("Copy folder from Release to Publish")]
    public class CopyFolderTask : CodedTaskBase
    {
        private CancellationToken _cancellationToken;
        private bool _shouldCancel;

        public override string Execute(CancellationToken cancellationToken)
        {
            this._cancellationToken = cancellationToken;

            int totalCopied = 0;
            for (int i = 0; i < 100 && !_shouldCancel; i++)
            {
                cancellationToken.ThrowIfCancellationRequested();

                UpdateStatusMessage("current i = " + i);

                ReportProgress(i + 1);

                Thread.Sleep(100);

                totalCopied++;
            }

            if (_shouldCancel)
            {
                _shouldCancel = false;
                _cancellationToken.ThrowIfCancellationRequested();
            }

            return $"Successfully copied {totalCopied} folders";
        }

        public override void Cancel()
        {
            _shouldCancel = true;
        }
    }

    [AttendedTask]
    public class CreateZipFileTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            Thread.Sleep(1000);

            return "Successfully made the zip file with 12 files";
        }
    }

    [Description("This is TaskBase D (Sample 1)")]
    public class TaskBaseD : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            const int x = 4;
            if (x < 6)
                throw new Exception(
                    "This is an intentional exception in order to simulate a situation when the TaskBase Fails.");
        }
    }

    [Description("Send Email to the Project Manager about Product Release (Sample 1)")]
    public class SendEmailTask : CodedTaskBase
    {
        public override string Execute(CancellationToken cancellationToken)
        {
            Thread.Sleep(1000);

            return "Successfully sent email to project.manager@hotmail.com";
        }
    }
}