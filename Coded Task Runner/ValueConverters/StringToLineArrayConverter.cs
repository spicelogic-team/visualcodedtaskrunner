﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace SpiceLogic.CodedTaskRunner.ValueConverters
{
    [ValueConversion(typeof(string), typeof(string[]))]
    internal class StringToLineArrayConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string theInputValue = (string) value;
            if (string.IsNullOrWhiteSpace(theInputValue))
                return new string[0];
            string[] theLines = theInputValue.Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            return theLines;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] theInput = (string[]) value;
            return string.Join(Environment.NewLine, theInput ?? throw new InvalidOperationException());
        }
    }
}