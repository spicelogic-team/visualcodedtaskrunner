﻿namespace SpiceLogic.CodedTaskRunner.Models
{
    public abstract class UserInputItem
    {
        public abstract string DisplayText { get; }            
    }
}