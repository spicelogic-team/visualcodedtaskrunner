﻿using System;

namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    internal class ExceptionViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionViewModel"/> class.
        /// </summary>
        public ExceptionViewModel()
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionViewModel" /> class.
        /// </summary>
        /// <param name="thrownException">The thrown exception.</param>
        /// <param name="windowTitle">The window title.</param>
        public ExceptionViewModel(Exception thrownException, string windowTitle)
        {
            this.ThrownException = new SingleExceptionViewModel(thrownException);
            Exception originalException = getOriginalException(thrownException);
            if (!thrownException.Equals(originalException))
            {
                this.OriginalException = new SingleExceptionViewModel(originalException);
                this.ShowOriginalException = true;
            }
            else
                this.ShowOriginalException = false;
            this.WindowTitle = windowTitle;
        }

        /// <summary>
        /// Gets the window title.
        /// </summary>
        public string WindowTitle { get; private set; }

        /// <summary>
        /// Gets the thrown exception.
        /// </summary>
        public SingleExceptionViewModel ThrownException { get; private set; }

        /// <summary>
        /// Gets the original exception.
        /// </summary>
        public SingleExceptionViewModel OriginalException { get; private set; }

        /// <summary>
        /// Gets a value indicating whether [show original exception].
        /// </summary>
        public bool ShowOriginalException { get; private set; }

        /// <summary>
        /// Gets the original exception.
        /// </summary>
        /// <param name="ex">The executable.</param>
        /// <returns></returns>
        private static Exception getOriginalException(Exception ex)
        {
            while (ex.InnerException != null)
                ex = ex.InnerException;
            return ex;
        }
    }
}