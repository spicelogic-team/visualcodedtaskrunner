﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using SpiceLogic.CodedTaskRunner.Models;

namespace SpiceLogic.CodedTaskRunner.ValueConverters
{
    internal class ResetStatusVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (MVVMCore.ViewModelBase.IsInDesignModeStatic)
                return Visibility.Visible;

            try
            {
                TaskRunStates overallProjectStatus = (TaskRunStates)values[0];
             
                bool isProjectDirty = (bool)values[1];

                return overallProjectStatus == TaskRunStates.Running
                    ? Visibility.Collapsed
                    : (isProjectDirty ? Visibility.Visible : Visibility.Collapsed);
            }
            catch
            {
                return Visibility.Visible;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] {""};
        }
     
    }
}