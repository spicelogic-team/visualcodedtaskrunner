﻿using SpiceLogic.CodedTaskRunner.MVVMCore;
using SpiceLogic.CodedTaskRunner.MVVMCore.Command;
using SpiceLogic.CodedTaskRunner.Utils;

namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    internal class AboutViewModel : ViewModelBase
    {
        private RelayCommand _okClickDialogCloseCommand;
        private bool? _dialogResult;

        #region --- Properties --

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        public static string ProductName => AssemblyInfo.Title;

        /// <summary>
        /// Gets the product version.
        /// </summary>
        public static string ProductVersion => AssemblyInfo.Version;

        /// <summary>
        /// Gets the copyright information.
        /// </summary>
        public static string CopyrightInfo => AssemblyInfo.Copyright;

        /// <summary>
        /// Gets the product description.
        /// </summary>
        public static string ProductDescription => AssemblyInfo.Description;

        /// <summary>
        /// Gets or sets the dialog result.
        /// </summary>
        public bool? DialogResult
        {
            get => _dialogResult;
            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion

        #region --- Commands ---

        /// <summary>
        /// Gets the ok click dialog close command.
        /// </summary>
        public RelayCommand OkClickDialogCloseCommand
        {
            get
            {
                return _okClickDialogCloseCommand ??
                      (_okClickDialogCloseCommand =  new RelayCommand(() => this.DialogResult = true));
            }
        }

        #endregion
    }
}