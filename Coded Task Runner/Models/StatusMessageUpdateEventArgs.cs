﻿using System;

namespace SpiceLogic.CodedTaskRunner.Models
{
    internal class StatusMessageUpdateEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the status message.
        /// </summary>
        public string StatusMessage { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusMessageUpdateEventArgs"/> class.
        /// </summary>
        /// <param name="statusMessage">The status message.</param>
        public StatusMessageUpdateEventArgs(string statusMessage)
        {
            this.StatusMessage = statusMessage;
        }
    }
}