﻿namespace SpiceLogic.CodedTaskRunner.ViewModels
{
    internal enum MessageNames
    {
        RestartApplicationForNewDLL,
        FocusAndSelectFirstTask,
        ScrollListToView
    }
}
