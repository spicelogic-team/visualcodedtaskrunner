﻿namespace SpiceLogic.CodedTaskRunner.MVVMCore.Messaging
{
    internal class ShowMessageBoxMessage : MessageBase
    {
        public ShowMessageBoxMessage(string message)
        {
            this.Message = message;
        }

        public string Message { get; private set; }
        public string Title { get; set; }
    }
}
